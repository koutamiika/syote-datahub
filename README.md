Kouta Datahub
=======

Fetch products from Visit Finland's Datahub service. Products are fetched based on region. You can fetch products from multiple regions. The plugin checks twice a day for any new products or content updates. This plugin is specifically made for [Syöte](https://syote.fi/) but works with any site. Plugin is in active development and in its current state, doesn't provide an "out of the box" solution for displaying products on your site as is lacks content blocks and/or shortcodes for display them. Using this plugin may require for you to modify your theme. Better support may or may not come later.

To use this plugin and Datahub API, you MUST register an user account as Open Data Consumer to [DataHub](https://datahub.visitfinland.com). After registering, you need to send an email to [datahub@visitfinland.com](datahub@visitfinland.com) and ask for API permission. You will receive an access token which you have to add to the plugin settings.
