# Copyright (C) 2023 Miika Salo
# This file is distributed under the same license as the Datahub plugin.
msgid ""
msgstr ""
"Project-Id-Version: Datahub 1.1.2\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/kouta-datahub\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2023-02-24T10:45:17+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: kouta-datahub\n"

#. Plugin Name of the plugin
#: includes/class-kouta-datahub-company.php:144
msgid "Datahub"
msgstr ""

#. Plugin URI of the plugin
#. Author URI of the plugin
msgid "https://koutamedia.fi"
msgstr ""

#. Description of the plugin
msgid "Fetch products from Visit Finland's Datahub API."
msgstr ""

#. Author of the plugin
msgid "Miika Salo"
msgstr ""

#: admin/partials/kouta-datahub-settings-view.php:18
msgid "Datahub Asetukset"
msgstr ""

#: admin/partials/kouta-datahub-settings-view.php:66
msgid "Automaattipäivitys on pois päältä. Tarkista asetukset, poista lisäosa käytöstä ja ota se uudelleen käyttöön."
msgstr ""

#: admin/partials/kouta-datahub-settings-view.php:72
msgid "Seuraava tarkistus: "
msgstr ""

#: includes/class-kouta-datahub-company.php:63
#: includes/class-kouta-datahub-company.php:64
msgid "Company"
msgstr ""

#: includes/class-kouta-datahub-company.php:65
#: includes/class-kouta-datahub-product.php:61
msgid "Add New"
msgstr ""

#: includes/class-kouta-datahub-company.php:66
msgid "Add New Company"
msgstr ""

#: includes/class-kouta-datahub-company.php:67
msgid "Edit Company"
msgstr ""

#: includes/class-kouta-datahub-company.php:68
msgid "New Company"
msgstr ""

#: includes/class-kouta-datahub-company.php:69
msgid "All Companies"
msgstr ""

#: includes/class-kouta-datahub-company.php:70
msgid "View Company"
msgstr ""

#: includes/class-kouta-datahub-company.php:71
msgid "Search Companies"
msgstr ""

#: includes/class-kouta-datahub-company.php:72
msgid "No Companies found"
msgstr ""

#: includes/class-kouta-datahub-company.php:73
msgid "No Companies found in Trash"
msgstr ""

#: includes/class-kouta-datahub-company.php:75
msgid "Companies"
msgstr ""

#: includes/class-kouta-datahub-company.php:85
msgid "company"
msgstr ""

#: includes/class-kouta-datahub-company.php:100
msgid "Meta"
msgstr ""

#: includes/class-kouta-datahub-company.php:145
msgid "Title"
msgstr ""

#: includes/class-kouta-datahub-company.php:146
msgid "Address"
msgstr ""

#: includes/class-kouta-datahub-company.php:147
msgid "Zip"
msgstr ""

#: includes/class-kouta-datahub-company.php:148
msgid "Email"
msgstr ""

#: includes/class-kouta-datahub-company.php:149
msgid "Phone"
msgstr ""

#: includes/class-kouta-datahub-company.php:150
msgid "Date"
msgstr ""

#: includes/class-kouta-datahub-company.php:158
msgid "Kyllä"
msgstr ""

#: includes/class-kouta-datahub-company.php:158
msgid "Manuaalinen"
msgstr ""

#: includes/class-kouta-datahub-integration.php:124
msgid "Missing or Invalid Access Token"
msgstr ""

#: includes/class-kouta-datahub-product.php:59
#: includes/class-kouta-datahub-product.php:60
msgid "Product"
msgstr ""

#: includes/class-kouta-datahub-product.php:62
msgid "Add New Product"
msgstr ""

#: includes/class-kouta-datahub-product.php:63
msgid "Edit Product"
msgstr ""

#: includes/class-kouta-datahub-product.php:64
msgid "New Product"
msgstr ""

#: includes/class-kouta-datahub-product.php:65
msgid "All Products"
msgstr ""

#: includes/class-kouta-datahub-product.php:66
msgid "View Product"
msgstr ""

#: includes/class-kouta-datahub-product.php:67
msgid "Search Products"
msgstr ""

#: includes/class-kouta-datahub-product.php:68
msgid "No Products found"
msgstr ""

#: includes/class-kouta-datahub-product.php:69
msgid "No products found in Trash"
msgstr ""

#: includes/class-kouta-datahub-product.php:71
msgid "Products"
msgstr ""

#: includes/class-kouta-datahub-product.php:81
msgid "products"
msgstr ""

#: includes/class-kouta-datahub-product.php:96
msgid "Product information"
msgstr ""

#: includes/kouta-datahub-functions.php:5
msgid "Monday"
msgstr ""

#: includes/kouta-datahub-functions.php:6
msgid "Tuesday"
msgstr ""

#: includes/kouta-datahub-functions.php:7
msgid "Wednesday"
msgstr ""

#: includes/kouta-datahub-functions.php:8
msgid "Thursday"
msgstr ""

#: includes/kouta-datahub-functions.php:9
msgid "Friday"
msgstr ""

#: includes/kouta-datahub-functions.php:10
msgid "Saturday"
msgstr ""

#: includes/kouta-datahub-functions.php:11
msgid "Sunday"
msgstr ""

#: includes/kouta-datahub-functions.php:12
msgid "Closed"
msgstr ""

#: includes/kouta-datahub-functions.php:13
msgid "Hours"
msgstr ""

#: includes/kouta-datahub-functions.php:14
msgid "Minutes"
msgstr ""

#: includes/kouta-datahub-functions.php:15
msgid "Days"
msgstr ""

#: includes/kouta-datahub-functions.php:81
msgid "Exceptional opening hours valid:"
msgstr ""

#: taxonomies/location.php:24
#: taxonomies/location.php:41
msgid "Locations"
msgstr ""

#: taxonomies/location.php:25
msgctxt "taxonomy general name"
msgid "Location"
msgstr ""

#: taxonomies/location.php:26
msgid "Search Locations"
msgstr ""

#: taxonomies/location.php:27
msgid "Popular Locations"
msgstr ""

#: taxonomies/location.php:28
msgid "All Locations"
msgstr ""

#: taxonomies/location.php:29
msgid "Parent Location"
msgstr ""

#: taxonomies/location.php:30
msgid "Parent Location:"
msgstr ""

#: taxonomies/location.php:31
msgid "Edit Location"
msgstr ""

#: taxonomies/location.php:32
msgid "Update Location"
msgstr ""

#: taxonomies/location.php:33
msgid "View Location"
msgstr ""

#: taxonomies/location.php:34
msgid "Add New Location"
msgstr ""

#: taxonomies/location.php:35
msgid "New Location"
msgstr ""

#: taxonomies/location.php:36
msgid "Separate Locations with commas"
msgstr ""

#: taxonomies/location.php:37
msgid "Add or remove Locations"
msgstr ""

#: taxonomies/location.php:38
msgid "Choose from the most used Locations"
msgstr ""

#: taxonomies/location.php:39
msgid "No Locations found."
msgstr ""

#: taxonomies/location.php:40
msgid "No Locations"
msgstr ""

#: taxonomies/location.php:42
msgid "Locations list navigation"
msgstr ""

#: taxonomies/location.php:43
msgid "Locations list"
msgstr ""

#: taxonomies/location.php:44
msgctxt "location"
msgid "Most Used"
msgstr ""

#: taxonomies/location.php:45
msgid "&larr; Back to Locations"
msgstr ""

#: taxonomies/location.php:65
msgid "Location added."
msgstr ""

#: taxonomies/location.php:66
msgid "Location deleted."
msgstr ""

#: taxonomies/location.php:67
msgid "Location updated."
msgstr ""

#: taxonomies/location.php:68
msgid "Location not added."
msgstr ""

#: taxonomies/location.php:69
msgid "Location not updated."
msgstr ""

#: taxonomies/location.php:70
msgid "Locations deleted."
msgstr ""

#: taxonomies/product_cat.php:24
#: taxonomies/product_cat.php:41
msgid "Categories"
msgstr ""

#: taxonomies/product_cat.php:25
msgctxt "taxonomy general name"
msgid "Category"
msgstr ""

#: taxonomies/product_cat.php:26
msgid "Search Categories"
msgstr ""

#: taxonomies/product_cat.php:27
msgid "Popular Categories"
msgstr ""

#: taxonomies/product_cat.php:28
msgid "All Categories"
msgstr ""

#: taxonomies/product_cat.php:29
msgid "Parent Category"
msgstr ""

#: taxonomies/product_cat.php:30
msgid "Parent Category:"
msgstr ""

#: taxonomies/product_cat.php:31
msgid "Edit Category"
msgstr ""

#: taxonomies/product_cat.php:32
msgid "Update Category"
msgstr ""

#: taxonomies/product_cat.php:33
msgid "View Category"
msgstr ""

#: taxonomies/product_cat.php:34
msgid "Add New Category"
msgstr ""

#: taxonomies/product_cat.php:35
msgid "New Category"
msgstr ""

#: taxonomies/product_cat.php:36
msgid "Separate Categories with commas"
msgstr ""

#: taxonomies/product_cat.php:37
msgid "Add or remove Categories"
msgstr ""

#: taxonomies/product_cat.php:38
msgid "Choose from the most used Categories"
msgstr ""

#: taxonomies/product_cat.php:39
msgid "No Categories found."
msgstr ""

#: taxonomies/product_cat.php:40
msgid "No Categories"
msgstr ""

#: taxonomies/product_cat.php:42
msgid "Categories list navigation"
msgstr ""

#: taxonomies/product_cat.php:43
msgid "Categories list"
msgstr ""

#: taxonomies/product_cat.php:44
msgctxt "product_cat"
msgid "Most Used"
msgstr ""

#: taxonomies/product_cat.php:45
msgid "&larr; Back to Categories"
msgstr ""

#: taxonomies/product_cat.php:65
msgid "Category added."
msgstr ""

#: taxonomies/product_cat.php:66
msgid "Category deleted."
msgstr ""

#: taxonomies/product_cat.php:67
msgid "Category updated."
msgstr ""

#: taxonomies/product_cat.php:68
msgid "Category not added."
msgstr ""

#: taxonomies/product_cat.php:69
msgid "Category not updated."
msgstr ""

#: taxonomies/product_cat.php:70
msgid "Categories deleted."
msgstr ""

#: taxonomies/product_type.php:24
#: taxonomies/product_type.php:41
msgid "Types"
msgstr ""

#: taxonomies/product_type.php:25
msgctxt "taxonomy general name"
msgid "Type"
msgstr ""

#: taxonomies/product_type.php:26
msgid "Search Types"
msgstr ""

#: taxonomies/product_type.php:27
msgid "Popular Types"
msgstr ""

#: taxonomies/product_type.php:28
msgid "All Types"
msgstr ""

#: taxonomies/product_type.php:29
msgid "Parent Type"
msgstr ""

#: taxonomies/product_type.php:30
msgid "Parent Type:"
msgstr ""

#: taxonomies/product_type.php:31
msgid "Edit Type"
msgstr ""

#: taxonomies/product_type.php:32
msgid "Update Type"
msgstr ""

#: taxonomies/product_type.php:33
msgid "View Type"
msgstr ""

#: taxonomies/product_type.php:34
msgid "Add New Type"
msgstr ""

#: taxonomies/product_type.php:35
msgid "New Type"
msgstr ""

#: taxonomies/product_type.php:36
msgid "Separate Types with commas"
msgstr ""

#: taxonomies/product_type.php:37
msgid "Add or remove Types"
msgstr ""

#: taxonomies/product_type.php:38
msgid "Choose from the most used Types"
msgstr ""

#: taxonomies/product_type.php:39
msgid "No Types found."
msgstr ""

#: taxonomies/product_type.php:40
msgid "No Types"
msgstr ""

#: taxonomies/product_type.php:42
msgid "Types list navigation"
msgstr ""

#: taxonomies/product_type.php:43
msgid "Types list"
msgstr ""

#: taxonomies/product_type.php:44
msgctxt "product_type"
msgid "Most Used"
msgstr ""

#: taxonomies/product_type.php:45
msgid "&larr; Back to Types"
msgstr ""

#: taxonomies/product_type.php:65
msgid "Type added."
msgstr ""

#: taxonomies/product_type.php:66
msgid "Type deleted."
msgstr ""

#: taxonomies/product_type.php:67
msgid "Type updated."
msgstr ""

#: taxonomies/product_type.php:68
msgid "Type not added."
msgstr ""

#: taxonomies/product_type.php:69
msgid "Type not updated."
msgstr ""

#: taxonomies/product_type.php:70
msgid "Types deleted."
msgstr ""

#: taxonomies/season.php:23
#: taxonomies/season.php:40
msgid "Seasons"
msgstr ""

#: taxonomies/season.php:24
msgctxt "taxonomy general name"
msgid "Season"
msgstr ""

#: taxonomies/season.php:25
msgid "Search Seasons"
msgstr ""

#: taxonomies/season.php:26
msgid "Popular Seasons"
msgstr ""

#: taxonomies/season.php:27
msgid "All Seasons"
msgstr ""

#: taxonomies/season.php:28
msgid "Parent Season"
msgstr ""

#: taxonomies/season.php:29
msgid "Parent Season:"
msgstr ""

#: taxonomies/season.php:30
msgid "Edit Season"
msgstr ""

#: taxonomies/season.php:31
msgid "Update Season"
msgstr ""

#: taxonomies/season.php:32
msgid "View Season"
msgstr ""

#: taxonomies/season.php:33
msgid "Add New Season"
msgstr ""

#: taxonomies/season.php:34
msgid "New Season"
msgstr ""

#: taxonomies/season.php:35
msgid "Separate Seasons with commas"
msgstr ""

#: taxonomies/season.php:36
msgid "Add or remove Seasons"
msgstr ""

#: taxonomies/season.php:37
msgid "Choose from the most used Seasons"
msgstr ""

#: taxonomies/season.php:38
msgid "No Seasons found."
msgstr ""

#: taxonomies/season.php:39
msgid "No Seasons"
msgstr ""

#: taxonomies/season.php:41
msgid "Seasons list navigation"
msgstr ""

#: taxonomies/season.php:42
msgid "Seasons list"
msgstr ""

#: taxonomies/season.php:43
msgctxt "season"
msgid "Most Used"
msgstr ""

#: taxonomies/season.php:44
msgid "&larr; Back to Seasons"
msgstr ""

#: taxonomies/season.php:64
msgid "Season added."
msgstr ""

#: taxonomies/season.php:65
msgid "Season deleted."
msgstr ""

#: taxonomies/season.php:66
msgid "Season updated."
msgstr ""

#: taxonomies/season.php:67
msgid "Season not added."
msgstr ""

#: taxonomies/season.php:68
msgid "Season not updated."
msgstr ""

#: taxonomies/season.php:69
msgid "Seasons deleted."
msgstr ""

#: taxonomies/target_group.php:23
#: taxonomies/target_group.php:40
msgid "Target Groups"
msgstr ""

#: taxonomies/target_group.php:24
msgctxt "taxonomy general name"
msgid "Target Group"
msgstr ""

#: taxonomies/target_group.php:25
msgid "Search Target Groups"
msgstr ""

#: taxonomies/target_group.php:26
msgid "Popular Target Groups"
msgstr ""

#: taxonomies/target_group.php:27
msgid "All Target Groups"
msgstr ""

#: taxonomies/target_group.php:28
msgid "Parent Target Group"
msgstr ""

#: taxonomies/target_group.php:29
msgid "Parent Target Group:"
msgstr ""

#: taxonomies/target_group.php:30
msgid "Edit Target Group"
msgstr ""

#: taxonomies/target_group.php:31
msgid "Update Target Group"
msgstr ""

#: taxonomies/target_group.php:32
msgid "View Target Group"
msgstr ""

#: taxonomies/target_group.php:33
msgid "Add New Target Group"
msgstr ""

#: taxonomies/target_group.php:34
msgid "New Target Group"
msgstr ""

#: taxonomies/target_group.php:35
msgid "Separate Target Groups with commas"
msgstr ""

#: taxonomies/target_group.php:36
msgid "Add or remove Target Groups"
msgstr ""

#: taxonomies/target_group.php:37
msgid "Choose from the most used Target Groups"
msgstr ""

#: taxonomies/target_group.php:38
msgid "No Target Groups found."
msgstr ""

#: taxonomies/target_group.php:39
msgid "No Target Groups"
msgstr ""

#: taxonomies/target_group.php:41
msgid "Target Groups list navigation"
msgstr ""

#: taxonomies/target_group.php:42
msgid "Target Groups list"
msgstr ""

#: taxonomies/target_group.php:43
msgctxt "target_group"
msgid "Most Used"
msgstr ""

#: taxonomies/target_group.php:44
msgid "&larr; Back to Target Groups"
msgstr ""

#: taxonomies/target_group.php:64
msgid "Target Group added."
msgstr ""

#: taxonomies/target_group.php:65
msgid "Target Group deleted."
msgstr ""

#: taxonomies/target_group.php:66
msgid "Target Group updated."
msgstr ""

#: taxonomies/target_group.php:67
msgid "Target Group not added."
msgstr ""

#: taxonomies/target_group.php:68
msgid "Target Group not updated."
msgstr ""

#: taxonomies/target_group.php:69
msgid "Target Groups deleted."
msgstr ""
