��    V      �     |      x     y     �     �     �     �     �     �     �               %     3     A  
   N     Y     m     }     �     �     �     �     �  $   �     	  	   "	     ,	     4	     9	     G	     T	     b	     o	      u	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     
     
     
     1
     D
     `
     r
     �
     �
     �
     �
     �
     �
     �
     �
                    (     :     K     \     l     �     �     �     �     �     �     �     �     �     �     
       	   %     /     3     ;     Q     Z     y     �  y  �     -     K     X     o     �     �     �     �     �     �                "  
   2     =     S     c     ~     �     �     �     �     �     
  	             $     4     G     Y     k     |     �  	   �     �  	   �  	   �  &   �  	   �     �            
   ,     7     G     b  $   z     �     �     �     �     �     �               0     G     M     Y     b     k     |     �     �     �     �  	   �     �     �     �     �               ,     >     M     ^     o     {     �     �     �  	   �     �     �         1       G   %                     *   5   E                                A   P                  H           !   6   -       8       #   M               ?      $       O   2   T      7       F   L       D   J         )   +                  @      >                   ,              R   .          4                   N      /      '   :              &   I   (      ;   	          V   
           "   U      B   3   9          C   =   Q         S          0   <   K        &larr; Back to Categories Add New Add New Category Add New Company Add New Location Add New Product Add or remove Categories Add or remove Locations Address All Categories All Companies All Locations All Products Categories Categories deleted. Categories list Categories list navigation Category added. Category deleted. Category not added. Category not updated. Category updated. Choose from the most used Categories Closed Companies Company Date Edit Category Edit Company Edit Location Edit Product Email Exceptional opening hours valid: Friday Hours Locations Minutes Missing or Invalid Access Token Monday New Category New Company New Location New Product No Categories No Categories found. No Companies found No Companies found in Trash No Products found No products found in Trash Parent Category Parent Category: Parent Location Parent Location: Phone Popular Categories Popular Locations Product Product information Products Saturday Search Categories Search Companies Search Locations Search Products Separate Categories with commas Separate Locations with commas Sunday Thursday Title Tuesday Types Update Category Update Location View Category View Company View Location View Product Wednesday Zip company product_catMost Used products taxonomy general nameCategory taxonomy general nameLocation taxonomy general nameType Project-Id-Version: Datahub 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/kouta-datahub
PO-Revision-Date: 2023-02-24 12:47+0200
Last-Translator: 
Language-Team: 
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Domain: kouta-datahub
Plural-Forms: nplurals=2; plural=(n != 1);
 &larr; Takaisin kategorioihin Lisää uusi Lisää uusi kategoria Lisää uusi yritys Lisää uusi sijainti Lisää uusi tuote Lisää tai poista kategoria Lisää tai poista sijainteja Osoite Kaikki kategoriat Kaikki yritykset Kaikki sijainnit Kaikki tuotteet Kategoriat Kategoriat poistettu. Kategoria lista Kategoria lista navigaatio Kategoria lisätty. Kategoria poistettu. Kategoriaa ei lisätty. Kategoriaa ei päivitetty. Kategoria päivitetty. Valitse käytetyimmistä Suljettu Yritykset Yritys Päivämäärä Muokkaa kategoriaa Muokkaa yritystä Muokkaa sijaintia Muokkaa tuotetta Sähköpostiosoite Aukioloajat voimassa: Perjantai Tuntia Sijainnit Minuuttia Puuttuva tai virheellinen Access Token Maanantai Lisää uusi kategoria Uusi yritys Uusi sijainti Uusi tuote Ei kategorioita Kategorioita ei löytynyt. Yrityksiä ei löytynyt Roskakorista ei löytynyt yrityksiä Tuotteita ei löytynyt Roskakori on tyhjä Vanhempi kategoria Vanhempi kategoria: Ylä-sijainti Ylä-sijainti: Puhelinnumero Suositut kategoriat Suosituimmat sijainnit Tuote Tuotetiedot Tuotteet Lauantai Hae kategorioita Hae yrityksiä Etsi sijainteja Hae tuotteita Erota kategoriat pilkulla Erota sijainnit pilkulla Sunnuntai Torstai Otsikko Tiistai Tyypit Päivitä kategoria Päivitä sijainti Näytä kategoria Näytä yritys Näytä sijainti Näytä tuotteet Keskiviikko Postinumero yritys Eniten käytetyt tuotteet Kategoria Sijainti Tyyppi 