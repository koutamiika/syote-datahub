<?php
/**
 * The Template for displaying all single purchase announcent
 *
 * This template can be overridden by copying it to yourtheme/single-dh_product.php.
 *
 * @package     kouta_datahub
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();
?>

<?php
if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<?php
	/**
	 * Kouta_datahub_before_main_content hook.
	 *
	 * @hooked kouta_datahub_output_content_wrapper - 10 (outputs opening divs for the content)
	 */
	do_action( 'kouta_datahub_before_main_content' );
?>

<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>

	<article id="datahub-<?php the_ID(); ?>" <?php post_class(); ?>>

		<header class="entry-header">
		<?php
		/**
		 * Hook: kouta_datahub_before_single_product_summary.
		 *
		 * @hooked datahub_template_single_title - 5
		 */
		do_action( 'kouta_datahub_before_single_product_summary' );
		?>
		</header>

		<div class="datahub-content entry-content">

			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail(); ?>
			<?php endif; ?>

			<?php
			/**
			 * Hook: woocommerce_single_product_summary.
			 *
			 * @hooked datahub_template_single_content - 5
			 */
			do_action( 'kouta_datahub_single_product_summary' );
			?>

			<?php
			/**
			 * Hook: kouta_datahub_single_product_meta.
			 *
			 * @hooked datahub_template_single_meta - 5
			 */
			do_action( 'kouta_datahub_single_product_meta' );
			?>

			<?php
			/**
			 * Hook: kouta_datahub_after_single_product_summary.
			 *
			 * @hooked load_product_images - 5
			 */
			do_action( 'kouta_datahub_after_single_product_summary' );
			?>

		</div>

	</article>

<?php endwhile; ?>

<?php
	/**
	 * Kouta_datahub_after_main_content hook.
	 *
	 * @hooked kouta_datahub_output_content_wrapper_end - 10 (outputs closing divs for the content)
	 */
	do_action( 'kouta_datahub_after_main_content' );
?>

<?php
get_footer();
