<?php

/**
 * Registers the `season` taxonomy,
 * for use with 'dh_product'.
 */
function season_init() {
	register_taxonomy( 'season', array( 'dh_product' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms' => 'edit_posts',
			'edit_terms'   => 'edit_posts',
			'delete_terms' => 'edit_posts',
			'assign_terms' => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Seasons', 'kouta-datahub' ),
			'singular_name'              => _x( 'Season', 'taxonomy general name', 'kouta-datahub' ),
			'search_items'               => __( 'Search Seasons', 'kouta-datahub' ),
			'popular_items'              => __( 'Popular Seasons', 'kouta-datahub' ),
			'all_items'                  => __( 'All Seasons', 'kouta-datahub' ),
			'parent_item'                => __( 'Parent Season', 'kouta-datahub' ),
			'parent_item_colon'          => __( 'Parent Season:', 'kouta-datahub' ),
			'edit_item'                  => __( 'Edit Season', 'kouta-datahub' ),
			'update_item'                => __( 'Update Season', 'kouta-datahub' ),
			'view_item'                  => __( 'View Season', 'kouta-datahub' ),
			'add_new_item'               => __( 'Add New Season', 'kouta-datahub' ),
			'new_item_name'              => __( 'New Season', 'kouta-datahub' ),
			'separate_items_with_commas' => __( 'Separate Seasons with commas', 'kouta-datahub' ),
			'add_or_remove_items'        => __( 'Add or remove Seasons', 'kouta-datahub' ),
			'choose_from_most_used'      => __( 'Choose from the most used Seasons', 'kouta-datahub' ),
			'not_found'                  => __( 'No Seasons found.', 'kouta-datahub' ),
			'no_terms'                   => __( 'No Seasons', 'kouta-datahub' ),
			'menu_name'                  => __( 'Seasons', 'kouta-datahub' ),
			'items_list_navigation'      => __( 'Seasons list navigation', 'kouta-datahub' ),
			'items_list'                 => __( 'Seasons list', 'kouta-datahub' ),
			'most_used'                  => _x( 'Most Used', 'season', 'kouta-datahub' ),
			'back_to_items'              => __( '&larr; Back to Seasons', 'kouta-datahub' ),
		),
		'show_in_rest'          => true,
		'rest_base'             => 'season',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'season_init' );

/**
 * Sets the post updated messages for the `season` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `season` taxonomy.
 */
function season_updated_messages( $messages ) {

	$messages['season'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Season added.', 'kouta-datahub' ),
		2 => __( 'Season deleted.', 'kouta-datahub' ),
		3 => __( 'Season updated.', 'kouta-datahub' ),
		4 => __( 'Season not added.', 'kouta-datahub' ),
		5 => __( 'Season not updated.', 'kouta-datahub' ),
		6 => __( 'Seasons deleted.', 'kouta-datahub' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'season_updated_messages' );
