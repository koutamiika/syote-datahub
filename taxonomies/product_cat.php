<?php

/**
 * Registers the `product_cat` taxonomy,
 * for use with 'dh_product'.
 */
function product_cat_init() {
	register_taxonomy( 'product_cat', array( 'dh_product' ), array(
		'publicly_queryable'    => false,
		'hierarchical'          => true,
		'public'                => true,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'rewrite'               => true,
		'capabilities'          => array(
			'manage_terms' => 'edit_posts',
			'edit_terms'   => 'edit_posts',
			'delete_terms' => 'edit_posts',
			'assign_terms' => 'edit_posts',
		),
		'labels'                => array(
			'name'                       => __( 'Categories', 'kouta-datahub' ),
			'singular_name'              => _x( 'Category', 'taxonomy general name', 'kouta-datahub' ),
			'search_items'               => __( 'Search Categories', 'kouta-datahub' ),
			'popular_items'              => __( 'Popular Categories', 'kouta-datahub' ),
			'all_items'                  => __( 'All Categories', 'kouta-datahub' ),
			'parent_item'                => __( 'Parent Category', 'kouta-datahub' ),
			'parent_item_colon'          => __( 'Parent Category:', 'kouta-datahub' ),
			'edit_item'                  => __( 'Edit Category', 'kouta-datahub' ),
			'update_item'                => __( 'Update Category', 'kouta-datahub' ),
			'view_item'                  => __( 'View Category', 'kouta-datahub' ),
			'add_new_item'               => __( 'Add New Category', 'kouta-datahub' ),
			'new_item_name'              => __( 'New Category', 'kouta-datahub' ),
			'separate_items_with_commas' => __( 'Separate Categories with commas', 'kouta-datahub' ),
			'add_or_remove_items'        => __( 'Add or remove Categories', 'kouta-datahub' ),
			'choose_from_most_used'      => __( 'Choose from the most used Categories', 'kouta-datahub' ),
			'not_found'                  => __( 'No Categories found.', 'kouta-datahub' ),
			'no_terms'                   => __( 'No Categories', 'kouta-datahub' ),
			'menu_name'                  => __( 'Categories', 'kouta-datahub' ),
			'items_list_navigation'      => __( 'Categories list navigation', 'kouta-datahub' ),
			'items_list'                 => __( 'Categories list', 'kouta-datahub' ),
			'most_used'                  => _x( 'Most Used', 'product_cat', 'kouta-datahub' ),
			'back_to_items'              => __( '&larr; Back to Categories', 'kouta-datahub' ),
		),
		'show_in_rest'          => true,
		'rest_base'             => 'product_cat',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'product_cat_init' );

/**
 * Sets the post updated messages for the `product_cat` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `product_cat` taxonomy.
 */
function product_cat_updated_messages( $messages ) {

	$messages['product_cat'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Category added.', 'kouta-datahub' ),
		2 => __( 'Category deleted.', 'kouta-datahub' ),
		3 => __( 'Category updated.', 'kouta-datahub' ),
		4 => __( 'Category not added.', 'kouta-datahub' ),
		5 => __( 'Category not updated.', 'kouta-datahub' ),
		6 => __( 'Categories deleted.', 'kouta-datahub' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'product_cat_updated_messages' );

function datahub_tag( $tag, $language ) {

	if ( empty( $tag ) ) {
		return;
	}

	$tags = array(
		'accommodation' => array(
			'en' => 'Accommodation',
			'fi' => 'Majoitus',
		),
		'hotels_hostels' => array(
			'en' => 'Hotels & Hostels',
			'fi' => 'Hotellit & hostellit',
		),
		'luxury_accommodation' => array(
			'en' => 'Luxury accommodation',
			'fi' => 'Luksusmajoitus',
		),
		'specialty_accommodation' => array( // SIC. Misspelled in datahub API.
			'en' => 'Speciality accommodation',
			'fi' => 'Erikoismajoitus',
		),
		'LGBTQ'=> array(
			'en' => 'LGBTQ+',
			'fi' => 'LGBTQ+',
		),
		'amusement_park'=> array(
			'en' => 'Amusement Park',
			'fi' => 'Huvipuisto',
		),
		'animal_parks_farms'=> array(
			'en' => 'Animal Parks & Farms',
			'fi' => 'Eläinpuistot ja maatilat',
		),
		'autumn_colours_ruska'=> array(
			'en' => 'Autumn Colours (Ruska)',
			'fi' => 'Syksyn värit (ruska)',
		),
		'bars_nightlife'=> array(
			'en' => 'Bars & Nightlife',
			'fi' => 'Baarit ja yöelämä',
		),
		'bed_breakfast_apartments_guest_house'=> array(
			'en' => 'Bed & Breakfast, Serviced Apartment, Guest House',
			'fi' => 'Aamiaismajoitus, huoneistohotelli, vierastalo',
		),
		'berry_mushroom_picking'=> array(
			'en' => 'Berry & Mushroom Picking',
			'fi' => 'Marjastus ja sienestys',
		),
		'boating_sailing'=> array(
			'en' => 'Boating & Sailing',
			'fi' => 'Veneily ja purjehdus',
		),
		'boutique'=> array(
			'en' => 'Boutique',
			'fi' => 'Putiikki',
		),
		'breweries_distilleries'=> array(
			'en' => 'Breweries & Distilleries',
			'fi' => 'Panimot ja tislaamot',
		),
		'business'=> array(
			'en' => 'Business',
			'fi' => 'Liiketoiminta',
		),
		'cafe'=> array(
			'en' => 'Café',
			'fi' => 'Kahvila',
		),
		'camp_school'=> array(
			'en' => 'Camp School',
			'fi' => 'Leirikoulu',
		),
		'camping'=> array(
			'en' => 'Camping',
			'fi' => 'Retkeily',
		),
		'celebration_venue'=> array(
			'en' => 'Celebration venue',
			'fi' => 'Juhlatila',
		),
		'childrens_attraction'=> array(
			'en' => 'Children\'s Attraction',
			'fi' => 'Lasten nähtävyydet',
		),
		'christmas_santa'=> array(
			'en' => 'Christmas & Santa',
			'fi' => 'Joulu & Joulupukki',
		),
		'climbing'=> array(
			'en' => 'Climbing',
			'fi' => 'Kiipeily',
		),
		'coast_archipelago'=> array(
			'en' => 'Coast & Archipelago',
			'fi' => 'Rannikko ja saaristo',
		),
		'cottages_villas'=> array(
			'en' => 'Cottages & Villas',
			'fi' => 'Mökit ja huvilat',
		),
		'creative_arts'=> array(
			'en' => 'Creative Arts',
			'fi' => 'Luovat taidot',
		),
		'cross_country_skiing'=> array(
			'en' => 'Cross-country Skiing',
			'fi' => 'Hiihto',
		),
		'cruises_ferries'=> array(
			'en' => 'Cruises & Ferries',
			'fi' => 'Risteilyt ja lautat',
		),
		'culinary_experience'=> array(
			'en' => 'Culinary Experience',
			'fi' => 'Kulinaarinen kokemus',
		),
		'cultural_heritage'=> array(
			'en' => 'Cultural Heritage',
			'fi' => 'Kulttuuriperintö',
		),
		'cycling_mountain_biking'=> array(
			'en' => 'Cycling & Mountain Biking',
			'fi' => 'Pyöräily ja maastopyöräily',
		),
		'day_trip'=> array(
			'en' => 'Day Trip',
			'fi' => 'Päivämatka',
		),
		'education'=> array(
			'en' => 'Education',
			'fi' => 'Oppiminen',
		),
		'event_venue'=> array(
			'en' => 'Event venue',
			'fi' => 'Tapahtumatila',
		),
		'events_festivals'=> array(
			'en' => 'Events & Festivals',
			'fi' => 'Tapahtumat ja festivaalit',
		),
		'family_activity'=> array(
			'en' => 'Family Activity',
			'fi' => 'Perheaktiviteetti',
		),
		'fast_food'=> array(
			'en' => 'Fast Food',
			'fi' => 'Pikaruoka',
		),
		'fine_dining'=> array(
			'en' => 'Fine Dining',
			'fi' => 'Fine Dining',
		),
		'finnish_design_fashion'=> array(
			'en' => 'Finnish Design and Fashion',
			'fi' => 'Suomalainen muotoilu ja muoti',
		),
		'fishing_hunting'=> array(
			'en' => 'Fishing & Hunting',
			'fi' => 'Kalastus ja metsästys',
		),
		'food_experience'=> array(
			'en' => 'Food Experience',
			'fi' => 'Ruokakokemus',
		),
		'forests'=> array(
			'en' => 'Forests',
			'fi' => 'Metsät',
		),
		'golf'=> array(
			'en' => 'Golf',
			'fi' => 'Golf',
		),
		'guidance'=> array(
			'en' => 'Guidance',
			'fi' => 'Opastus',
		),
		'guided_service'=> array(
			'en' => 'Guided Service',
			'fi' => 'Opastettu palvelu',
		),
		'handicraft'=> array(
			'en' => 'Handicraft',
			'fi' => 'Käsityö',
		),
		'hiking_walking_trekking'=> array(
			'en' => 'Hiking, Walking & Trekking',
			'fi' => 'Vaellus ja kävely',
		),
		'historical_sites'=> array(
			'en' => 'Historical Sites',
			'fi' => 'Historialliset paikat',
		),
		'history'=> array(
			'en' => 'History',
			'fi' => 'Historia',
		),
		'horses'=> array(
			'en' => 'Horses',
			'fi' => 'Hevoset',
		),
		'huskies'=> array(
			'en' => 'Huskies',
			'fi' => 'Huskyt',
		),
		'ice_climbing'=> array(
			'en' => 'Ice Climbing',
			'fi' => 'Jääkiipeily',
		),
		'ice_fishing'=> array(
			'en' => 'Ice Fishing',
			'fi' => 'Pilkkiminen',
		),
		'ice_skating'=> array(
			'en' => 'Ice Skating',
			'fi' => 'Luistelu',
		),
		'ice_swimming'=> array(
			'en' => 'Ice Swimming',
			'fi' => 'Avantouinti',
		),
		'lakes'=> array(
			'en' => 'Lakes',
			'fi' => 'Järvet',
		),
		'local_food'=> array(
			'en' => 'Local Food',
			'fi' => 'Paikallinen ruoka',
		),
		'local_lifestyle'=> array(
			'en' => 'Local Lifestyle',
			'fi' => 'Paikallinen elämäntapa',
		),
		'local_products'=> array(
			'en' => 'Local Products',
			'fi' => 'Paikalliset tuotteet',
		),
		'luxury'=> array(
			'en' => 'Luxury',
			'fi' => 'Luksus',
		),
		'luxury_accommodation'=> array(
			'en' => 'Luxury Accommodation',
			'fi' => 'Luksusmajoitus',
		),
		'market'=> array(
			'en' => 'Market',
			'fi' => 'Tori',
		),
		'medical_service'=> array(
			'en' => 'Medical Service',
			'fi' => 'Lääkintäpalvelut',
		),
		'meeting_venue'=> array(
			'en' => 'Meeting venue',
			'fi' => 'Kokoustila',
		),
		'midnight_sun'=> array(
			'en' => 'Midnight Sun',
			'fi' => 'Keskiyön aurinko',
		),
		'motorsports'=> array(
			'en' => 'Motorsports',
			'fi' => 'Moottoriurheilu',
		),
		'museums_galleries'=> array(
			'en' => 'Museums & Galleries',
			'fi' => 'Museot ja galleriat',
		),
		'music'=> array(
			'en' => 'Music',
			'fi' => 'Musiikki',
		),
		'mystay'=> array(
			'en' => 'My Stay',
			'fi' => 'My Stay',
		),
		'national_park'=> array(
			'en' => 'National Park',
			'fi' => 'Kansallispuisto',
		),
		'natural_site'=> array(
			'en' => 'Natural Site',
			'fi' => 'Luontokohde',
		),
		'nature_excursion'=> array(
			'en' => 'Nature Excursion',
			'fi' => 'Luontoretki',
		),
		'northern_lights'=> array(
			'en' => 'Northern Lights',
			'fi' => 'Revontulet',
		),
		'orienteering'=> array(
			'en' => 'Orienteering',
			'fi' => 'Suunnistus',
		),
		'other_activity'=> array(
			'en' => 'Other Activity',
			'fi' => 'Muu aktiviteetti',
		),
		'other_attraction'=> array(
			'en' => 'Other Attraction',
			'fi' => 'Muu nähtävyys',
		),
		'other_shop'=> array(
			'en' => 'Other Shop',
			'fi' => 'Muu kauppa',
		),
		'other_winter_activity'=> array(
			'en' => 'Other Winter Activity',
			'fi' => 'Muu talviaktiviteetti',
		),
		'outlet'=> array(
			'en' => 'Outlet',
			'fi' => 'Outletti',
		),
		'paddling_rafting'=> array(
			'en' => 'Paddling & Rafting',
			'fi' => 'Melonta ja koskenlasku',
		),
		'parks_gardens'=> array(
			'en' => 'Parks & Gardens',
			'fi' => 'Puistot ja puutarhat',
		),
		'pet_friendly'=> array(
			'en' => 'Pet Friendly',
			'fi' => 'Lemmikkiystävällinen',
		),
		'photography'=> array(
			'en' => 'Photography',
			'fi' => 'Valokuvaus',
		),
		'private_experience'=> array(
			'en' => 'Private Experience',
			'fi' => 'Yksityinen kokemus',
		),
		'private_restaurant'=> array(
			'en' => 'Private restaurant',
			'fi' => 'Yksityinen ravintila',
		),
		'rehabilitation'=> array(
			'en' => 'Rehabilitation',
			'fi' => 'Kuntoutus',
		),
		'reindeer'=> array(
			'en' => 'Reindeer',
			'fi' => 'Poro',
		),
		'restaurant'=> array(
			'en' => 'Restaurant',
			'fi' => 'Ravintola',
		),
		'running'=> array(
			'en' => 'Running',
			'fi' => 'Juoksu',
		),
		'running_trailrunning'=> array(
			'en' => 'Running & Trailrunning',
			'fi' => 'Juoksu ja polkujuoksu',
		),
		'safari'=> array(
			'en' => 'Safari',
			'fi' => 'Safari',
		),
		'sauna_experience'=> array(
			'en' => 'Sauna Experience',
			'fi' => 'Saunakokemus',
		),
		'scenic_point'=> array(
			'en' => 'Scenic Point',
			'fi' => 'Näköalapaikka',
		),
		'school_visit'=> array(
			'en' => 'School Visit',
			'fi' => 'Kouluvierailu',
		),
		'shopping_center'=> array(
			'en' => 'Shopping Center',
			'fi' => 'Kauppakeskus',
		),
		'sightseeing_tours'=> array(
			'en' => 'Sightseeing & Tours',
			'fi' => 'Kiertoajelut ja retket',
		),
		'silence_program'=> array(
			'en' => 'Silence Program',
			'fi' => 'Hiljaisuus-ohjelma',
		),
		'ski_resort'=> array(
			'en' => 'Ski Resort',
			'fi' => 'Laskettelukeskus',
		),
		'ski_school'=> array(
			'en' => 'Ski School',
			'fi' => 'Laskettelukoulu',
		),
		'skiing_snowboarding'=> array(
			'en' => 'Skiing & Snowboarding',
			'fi' => 'Hiihto ja lumilautailu',
		),
		'snowmobiling'=> array(
			'en' => 'Snowmobiling',
			'fi' => 'Moottorikelkkailu',
		),
		'snowshoeing'=> array(
			'en' => 'Snowshoeing',
			'fi' => 'Lumikenkäily',
		),
		'souvenirs'=> array(
			'en' => 'Souvenirs',
			'fi' => 'Matkamuistot',
		),
		'spa_recreational_spa'=> array(
			'en' => 'Spa & Recreational Spa',
			'fi' => 'Spa- ja virkistyskylpylä',
		),
		'sports'=> array(
			'en' => 'Sports',
			'fi' => 'Urheilu',
		),
		'supermarket'=> array(
			'en' => 'Supermarket',
			'fi' => 'Supermarket',
		),
		'swimming'=> array(
			'en' => 'Swimming',
			'fi' => 'Uiminen',
		),
		'tourist_information'=> array(
			'en' => 'Tourist Information',
			'fi' => 'Turisti-info',
		),
		'transportation'=> array(
			'en' => 'Transportation',
			'fi' => 'Kuljetus',
		),
		'vegetarian_vegan'=> array(
			'en' => 'Vegetarian/Vegan',
			'fi' => 'Kasvissyönti/Vegaani',
		),
		'virtual_reality'=> array(
			'en' => 'Virtual Reality',
			'fi' => 'Virtuaalitodellisuus',
		),
		'water_activities'=> array(
			'en' => 'Water Activities',
			'fi' => 'Vesiaktiviteetit',
		),
		'wellbeing_from_nature'=> array(
			'en' => 'Wellbeing from Nature',
			'fi' => 'Hyvinvointi luonnosta',
		),
		'wellness_treatments'=> array(
			'en' => 'Wellness Treatments',
			'fi' => 'Hyvinvointihoidot',
		),
		'wildlife_bird_watching'=> array(
			'en' => 'Wildlife & Bird Watching',
			'fi' => 'Villieläimet ja lintujen tarkkailu',
		),
		'winter_biking'=> array(
			'en' => 'Winter Biking',
			'fi' => 'Talvi pyöräily',
		),
		'yoga_meditation'=> array(
			'en' => 'Yoga & Meditation',
			'fi' => 'Jooga ja meditaatio',
		),
	);

	$tags = apply_filters( 'datahub_tags_array', $tags );

	if ( ! array_key_exists( $tag, $tags ) ) {
		return $tag;
	}

	return $tags[ $tag ][ $language ];

}