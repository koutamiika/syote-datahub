<?php

/**
 * Registers the `product_type` taxonomy,
 * for use with 'dh_product'.
 */
function product_type_init() {
	register_taxonomy( 'product_type', array( 'dh_product' ), array(
		'publicly_queryable'    => false,
		'hierarchical'          => true,
		'public'                => true,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'rewrite'               => true,
		'capabilities'          => array(
			'manage_terms' => 'edit_posts',
			'edit_terms'   => 'edit_posts',
			'delete_terms' => 'edit_posts',
			'assign_terms' => 'edit_posts',
		),
		'labels'                => array(
			'name'                       => __( 'Types', 'kouta-datahub' ),
			'singular_name'              => _x( 'Type', 'taxonomy general name', 'kouta-datahub' ),
			'search_items'               => __( 'Search Types', 'kouta-datahub' ),
			'popular_items'              => __( 'Popular Types', 'kouta-datahub' ),
			'all_items'                  => __( 'All Types', 'kouta-datahub' ),
			'parent_item'                => __( 'Parent Type', 'kouta-datahub' ),
			'parent_item_colon'          => __( 'Parent Type:', 'kouta-datahub' ),
			'edit_item'                  => __( 'Edit Type', 'kouta-datahub' ),
			'update_item'                => __( 'Update Type', 'kouta-datahub' ),
			'view_item'                  => __( 'View Type', 'kouta-datahub' ),
			'add_new_item'               => __( 'Add New Type', 'kouta-datahub' ),
			'new_item_name'              => __( 'New Type', 'kouta-datahub' ),
			'separate_items_with_commas' => __( 'Separate Types with commas', 'kouta-datahub' ),
			'add_or_remove_items'        => __( 'Add or remove Types', 'kouta-datahub' ),
			'choose_from_most_used'      => __( 'Choose from the most used Types', 'kouta-datahub' ),
			'not_found'                  => __( 'No Types found.', 'kouta-datahub' ),
			'no_terms'                   => __( 'No Types', 'kouta-datahub' ),
			'menu_name'                  => __( 'Types', 'kouta-datahub' ),
			'items_list_navigation'      => __( 'Types list navigation', 'kouta-datahub' ),
			'items_list'                 => __( 'Types list', 'kouta-datahub' ),
			'most_used'                  => _x( 'Most Used', 'product_type', 'kouta-datahub' ),
			'back_to_items'              => __( '&larr; Back to Types', 'kouta-datahub' ),
		),
		'show_in_rest'          => true,
		'rest_base'             => 'product_type',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'product_type_init' );

/**
 * Sets the post updated messages for the `product_type` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `product_type` taxonomy.
 */
function product_type_updated_messages( $messages ) {

	$messages['product_type'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Type added.', 'kouta-datahub' ),
		2 => __( 'Type deleted.', 'kouta-datahub' ),
		3 => __( 'Type updated.', 'kouta-datahub' ),
		4 => __( 'Type not added.', 'kouta-datahub' ),
		5 => __( 'Type not updated.', 'kouta-datahub' ),
		6 => __( 'Types deleted.', 'kouta-datahub' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'product_type_updated_messages' );
