<?php

/**
 * Registers the `target_group` taxonomy,
 * for use with 'dh_product'.
 */
function target_group_init() {
	register_taxonomy( 'target_group', array( 'dh_product' ), array(
		'hierarchical'      => false,
		'public'            => false,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Target Groups', 'kouta-datahub' ),
			'singular_name'              => _x( 'Target Group', 'taxonomy general name', 'kouta-datahub' ),
			'search_items'               => __( 'Search Target Groups', 'kouta-datahub' ),
			'popular_items'              => __( 'Popular Target Groups', 'kouta-datahub' ),
			'all_items'                  => __( 'All Target Groups', 'kouta-datahub' ),
			'parent_item'                => __( 'Parent Target Group', 'kouta-datahub' ),
			'parent_item_colon'          => __( 'Parent Target Group:', 'kouta-datahub' ),
			'edit_item'                  => __( 'Edit Target Group', 'kouta-datahub' ),
			'update_item'                => __( 'Update Target Group', 'kouta-datahub' ),
			'view_item'                  => __( 'View Target Group', 'kouta-datahub' ),
			'add_new_item'               => __( 'Add New Target Group', 'kouta-datahub' ),
			'new_item_name'              => __( 'New Target Group', 'kouta-datahub' ),
			'separate_items_with_commas' => __( 'Separate Target Groups with commas', 'kouta-datahub' ),
			'add_or_remove_items'        => __( 'Add or remove Target Groups', 'kouta-datahub' ),
			'choose_from_most_used'      => __( 'Choose from the most used Target Groups', 'kouta-datahub' ),
			'not_found'                  => __( 'No Target Groups found.', 'kouta-datahub' ),
			'no_terms'                   => __( 'No Target Groups', 'kouta-datahub' ),
			'menu_name'                  => __( 'Target Groups', 'kouta-datahub' ),
			'items_list_navigation'      => __( 'Target Groups list navigation', 'kouta-datahub' ),
			'items_list'                 => __( 'Target Groups list', 'kouta-datahub' ),
			'most_used'                  => _x( 'Most Used', 'target_group', 'kouta-datahub' ),
			'back_to_items'              => __( '&larr; Back to Target Groups', 'kouta-datahub' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'target_group',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'target_group_init' );

/**
 * Sets the post updated messages for the `target_group` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `target_group` taxonomy.
 */
function target_group_updated_messages( $messages ) {

	$messages['target_group'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Target Group added.', 'kouta-datahub' ),
		2 => __( 'Target Group deleted.', 'kouta-datahub' ),
		3 => __( 'Target Group updated.', 'kouta-datahub' ),
		4 => __( 'Target Group not added.', 'kouta-datahub' ),
		5 => __( 'Target Group not updated.', 'kouta-datahub' ),
		6 => __( 'Target Groups deleted.', 'kouta-datahub' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'target_group_updated_messages' );
