<?php

/**
 * Registers the `location` taxonomy,
 * for use with 'dh_product'.
 */
function location_init() {
	register_taxonomy( 'location', array( 'dh_product' ), array(
		'publicly_queryable'    => false,
		'hierarchical'          => true,
		'public'                => true,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'rewrite'               => true,
		'capabilities'          => array(
			'manage_terms' => 'edit_posts',
			'edit_terms'   => 'edit_posts',
			'delete_terms' => 'edit_posts',
			'assign_terms' => 'edit_posts',
		),
		'labels'                => array(
			'name'                       => __( 'Locations', 'kouta-datahub' ),
			'singular_name'              => _x( 'Location', 'taxonomy general name', 'kouta-datahub' ),
			'search_items'               => __( 'Search Locations', 'kouta-datahub' ),
			'popular_items'              => __( 'Popular Locations', 'kouta-datahub' ),
			'all_items'                  => __( 'All Locations', 'kouta-datahub' ),
			'parent_item'                => __( 'Parent Location', 'kouta-datahub' ),
			'parent_item_colon'          => __( 'Parent Location:', 'kouta-datahub' ),
			'edit_item'                  => __( 'Edit Location', 'kouta-datahub' ),
			'update_item'                => __( 'Update Location', 'kouta-datahub' ),
			'view_item'                  => __( 'View Location', 'kouta-datahub' ),
			'add_new_item'               => __( 'Add New Location', 'kouta-datahub' ),
			'new_item_name'              => __( 'New Location', 'kouta-datahub' ),
			'separate_items_with_commas' => __( 'Separate Locations with commas', 'kouta-datahub' ),
			'add_or_remove_items'        => __( 'Add or remove Locations', 'kouta-datahub' ),
			'choose_from_most_used'      => __( 'Choose from the most used Locations', 'kouta-datahub' ),
			'not_found'                  => __( 'No Locations found.', 'kouta-datahub' ),
			'no_terms'                   => __( 'No Locations', 'kouta-datahub' ),
			'menu_name'                  => __( 'Locations', 'kouta-datahub' ),
			'items_list_navigation'      => __( 'Locations list navigation', 'kouta-datahub' ),
			'items_list'                 => __( 'Locations list', 'kouta-datahub' ),
			'most_used'                  => _x( 'Most Used', 'location', 'kouta-datahub' ),
			'back_to_items'              => __( '&larr; Back to Locations', 'kouta-datahub' ),
		),
		'show_in_rest'          => true,
		'rest_base'             => 'location',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'location_init' );

/**
 * Sets the post updated messages for the `location` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `location` taxonomy.
 */
function location_updated_messages( $messages ) {

	$messages['location'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Location added.', 'kouta-datahub' ),
		2 => __( 'Location deleted.', 'kouta-datahub' ),
		3 => __( 'Location updated.', 'kouta-datahub' ),
		4 => __( 'Location not added.', 'kouta-datahub' ),
		5 => __( 'Location not updated.', 'kouta-datahub' ),
		6 => __( 'Locations deleted.', 'kouta-datahub' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'location_updated_messages' );
