<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://koutamedia.fi
 * @since             1.0.0
 * @package           Kouta_Datahub
 *
 * @wordpress-plugin
 * Plugin Name:       Datahub
 * Plugin URI:        https://koutamedia.fi
 * Description:       Fetch products from Visit Finland's Datahub API.
 * Version:           1.2.0
 * Author:            Miika Salo
 * Author URI:        https://koutamedia.fi
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       kouta-datahub
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'KOUTA_DATAHUB_VERSION', '1.2.0' );

define( 'KOUTA_DATAHUB_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-kouta-datahub-activator.php
 */
function activate_kouta_datahub() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-kouta-datahub-activator.php';
	Kouta_Datahub_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-kouta-datahub-deactivator.php
 */
function deactivate_kouta_datahub() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-kouta-datahub-deactivator.php';
	Kouta_Datahub_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_kouta_datahub' );
register_deactivation_hook( __FILE__, 'deactivate_kouta_datahub' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-kouta-datahub.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_kouta_datahub() {

	$plugin = new Kouta_Datahub();
	$plugin->run();

}
run_kouta_datahub();
