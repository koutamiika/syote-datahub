(function( $ ) {
	'use strict';

	jQuery(document).ready(function($){
		$('#datahub-connect').click(function(e) {
			e.preventDefault();

			$.ajax({
				type: 'GET',
				url: passed.ajaxurl,
				data: {
					'action': 'get_datahub_access_token',
				},
				success: function(result) {
					console.log(result)
					location.reload();
				}
			});
		});

		$('#fetch').click(function(e) {
			e.preventDefault();
			console.log( passed.completed );
			$.ajax({
				type: 'GET',
				url: passed.ajaxurl,
				data: {
					'action': 'get_datahub_products',
				},
				beforeSend: function() {
					$('#fetch .spinner').css('display', 'inline-block');
					$('#fetch .spinner').css('visibility', 'visible');
				},
				success: function(result, type, xhr) {
					console.log(result)
					console.log(type)
					console.log(xhr)
					if ( false === result.success ) {
						alert(result.data.message)
					} else {
						$('.messages').html(result.message)
					}
				},
				complete: function( data ) {
					console.log(data);

					$('#fetch .spinner').css('display', 'none');
					$('#fetch .spinner').css('visibility', 'hidden');
				}
			});
		});
	})

})( jQuery );
