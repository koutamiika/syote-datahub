<?php
/**
 * Product Meta fields HTML
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 */

?>

<div class="wrap">
	<?php
	$args = array(
		'post_type'   => 'dh_product',
		'numberposts' => -1,
		'meta_query' => array(
			array(
				'key' => 'business_id',
				'value' => get_post_meta( $post->ID, 'business_id', true )
			)
		)
	);
	$products = get_posts( $args );
	?>
    <table class="form-table">
        <?php foreach ( $products as $product ) : ?>
        <tr valign="top">
            <th scope="row"><?php echo get_the_title( $product->ID ); ?></th>
        </tr>
        <?php endforeach; ?>
    </table>
</div>