<?php
/**
 * Product Meta fields HTML
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 */

?>

<div class="wrap">

    <table class="form-table">
        <tr valign="top">
            <th scope="row">Datahub ID</th>
            <td><?php echo get_post_meta( $post->ID, 'datahub_id', true ); ?></td>
        </tr>
        <tr valign="top">
            <th scope="row">Business ID</th>
            <td><input type="text" class="widefat" name="business_id" id="business_id" value="<?php echo get_post_meta( $post->ID, 'business_id', true ); ?>"></td>
        </tr>
        <tr valign="top">
            <th scope="row">Updated date</th>
            <td><input type="text" class="widefat" name="updated" id="updated" value="<?php echo get_post_meta( $post->ID, 'updatedAt', true ); ?>"></td>
        </tr>
        <tr valign="top">
            <th scope="row">Webshop URL</th>
            <td><input type="url" class="widefat" name="webshop" id="webshop" value="<?php echo get_post_meta( $post->ID, 'webshop_url', true ); ?>"></td>
        </tr>
        <tr valign="top">
            <th scope="row">Website URL</th>
            <td><input type="url" class="widefat" name="website" id="website" value="<?php echo get_post_meta( $post->ID, 'website_url', true ); ?>"></td>
        </tr>
        <tr valign="top">
            <th scope="row">Duration</th>
            <td><input type="text" class="widefat" name="duration" id="duration" value="<?php echo get_post_meta( $post->ID, 'duration', true ); ?>"></td>
        </tr>
        <tr valign="top">
            <th scope="row">Duration type</th>
            <td><input type="text" class="widefat" name="duration_type" id="duration_type" value="<?php echo get_post_meta( $post->ID, 'duration_type', true ); ?>"></td>
        </tr>
        <tr valign="top">
            <th scope="row">Opening Hours (deprecated)</th>
            <td><input type="text" class="widefat" name="opening_hours" id="opening_hours" value="<?php echo htmlspecialchars( json_encode( get_post_meta( $post->ID, 'opening_hours', true ) ) ); ?>"></td>
        </tr>
        <tr valign="top">
            <th scope="row">Business Hours</th>
            <td><input type="text" class="widefat" name="business_hours" id="business_hours" value="<?php echo htmlspecialchars( json_encode( get_post_meta( $post->ID, 'business_hours', true ) ) ); ?>"></td>
        </tr>
		<tr valign="top">
            <th scope="row">Coordinates</th>
            <td><input type="text" class="widefat" name="coordinates" id="coordinates" value="<?php echo get_post_meta( $post->ID, 'coordinates', true ); ?>"></td>
        </tr>
		<tr valign="top">
            <th scope="row">Phone</th>
            <td><input type="text" class="widefat" name="phone" id="phone" value="<?php echo get_post_meta( $post->ID, 'phone', true ); ?>"></td>
        </tr>
		<tr valign="top">
            <th scope="row">Email</th>
            <td><input type="email" class="widefat" name="email" id="email" value="<?php echo get_post_meta( $post->ID, 'email', true ); ?>"></td>
        </tr>
		<tr valign="top">
            <th scope="row">Address</th>
            <td><input type="text" class="widefat" name="address" id="address" value="<?php echo get_post_meta( $post->ID, 'address', true ); ?>"></td>
        </tr>
		<tr valign="top">
            <th scope="row">Zip</th>
            <td><input type="text" class="widefat" name="zip" id="zip" value="<?php echo get_post_meta( $post->ID, 'zip', true ); ?>"></td>
        </tr>
		<tr valign="top">
            <th scope="row">From Price</th>
            <td><input type="text" class="widefat" name="from_price" id="from_price" value="<?php echo get_post_meta( $post->ID, 'from_price', true ); ?>"></td>
        </tr>
		<tr valign="top">
            <th scope="row">To Price</th>
            <td><input type="text" class="widefat" name="to_price" id="to_price" value="<?php echo get_post_meta( $post->ID, 'to_price', true ); ?>"></td>
        </tr>
		<tr valign="top">
            <th scope="row">Accessible</th>
            <td><input type="text" class="widefat" name="accessible" id="accessible" value="<?php echo get_post_meta( $post->ID, 'accessible', true ); ?>"></td>
        </tr>
		<tr valign="top">
            <th scope="row">Certificates</th>
            <td><input type="text" class="widefat" name="certificates" id="certificates" value="<?php echo htmlspecialchars( json_encode( get_post_meta( $post->ID, 'certificates', true ) ) ); ?>"></td>
        </tr>
		<tr valign="top">
            <th scope="row">Capacities</th>
            <td><input type="text" class="widefat" name="capacities" id="capacities" value="<?php echo htmlspecialchars( json_encode( get_post_meta( $post->ID, 'capacities', true ) ) ); ?>"></td>
        </tr>
		<tr valign="top">
            <th scope="row">Social links</th>
            <td><input type="text" class="widefat" name="social_links" id="social_links" value="<?php echo htmlspecialchars( json_encode( get_post_meta( $post->ID, 'social_links', true ) ) ); ?>"></td>
        </tr>
		capacities

		<tr valign="top">
            <th scope="row">Videos</th>
            <td><input type="text" class="widefat" name="videos" id="videos" value="<?php echo get_post_meta( $post->ID, 'productVideos', true ); ?>"></td>
        </tr>
		<tr valign="top">
			<?php $images = get_attached_media( 'image', $post );?>
            <th scope="row">Images</th>
            <td>
				<?php if ( $images ) : ?>
					<?php foreach( $images as $image ) : ?>
						<?php echo wp_get_attachment_image( $image->ID, 'thumbnail' ); ?>
					<?php endforeach; ?>
				<?php endif; ?>
			</td>
        </tr>
    </table>

</div>