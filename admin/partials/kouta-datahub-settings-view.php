<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/admin/partials
 */
?>

<div class="wrap">

	<h1><?php esc_html_e( 'Datahub Asetukset', 'kouta-datahub' ); ?></h1>

	<form method="post" action="options.php">

		<?php settings_fields( 'kouta-datahub-settings' ); ?>
		<?php do_settings_sections( 'kouta-datahub-settings' ); ?>

		<table class="form-table">
			<tr valign="top">
				<th scope="row">Access key <span class="required">*</span></th>
				<td><input type="password" class="widefat" name="kouta-datahub-access-key" value="<?php echo esc_attr( get_option( 'kouta-datahub-access-key' ) ); ?>" required="required" /></td>
			</tr>

			<tr valign="top">
				<th scope="row">Username <span class="required">*</span></th>
				<td><input type="text" class="widefat" name="kouta-datahub-api-username" value="<?php echo esc_attr( get_option( 'kouta-datahub-api-username' ) ); ?>" required="required" /></td>
			</tr>

			<tr valign="top">
				<th scope="row">Password <span class="required">*</span></th>
				<td><input type="password" class="widefat" name="kouta-datahub-api-password" value="<?php echo esc_attr( get_option( 'kouta-datahub-api-password' ) ); ?>" required="required" /></td>
			</tr>

			<tr valign="top">
				<th scope="row">Token</th>
				<td><input type="text" class="widefat" name="kouta-datahub-api-token" value="<?php echo esc_attr( get_option( 'kouta-datahub-api-token' ) ); ?>" /></td>
			</tr>

			<tr valign="top">
				<th scope="row">Datahub UUIDs</th>
				<td><textarea class="widefat" name="kouta-datahub-product-uuids" id="kouta-datahub-product-uuids" cols="30" rows="10"><?php echo esc_attr( get_option( 'kouta-datahub-product-uuids' ) ); ?></textarea></td>
			</tr>

			<tr valign="top">
				<th scope="row">Force update</th>
				<td><input type="checkbox" name="kouta-datahub-force-update" value="1" <?php checked(1, get_option( 'kouta-datahub-force-update' ), true ); ?> /></td>
			</tr>

		</table>

        <div class="button-group clearfix">
            <?php submit_button(); ?>
            <a id="datahub-connect" class="button">Yhdistä</a>
			<a id="fetch" class="button">Tuo tuotteet <span class="spinner" style="display: none;"></span></a>
        </div>

		<div class="next-schedule">
		<?php if ( ! wp_next_scheduled( 'datahub_api_call_event' ) ) : ?>
			<p><?php esc_html_e( 'Automaattipäivitys on pois päältä. Tarkista asetukset, poista lisäosa käytöstä ja ota se uudelleen käyttöön.', 'kouta-datahub' ); ?></p>
		<?php else : ?>
			<?php
			$date = new DateTime();
			$date->setTimestamp( wp_next_scheduled( 'datahub_api_call_event' ) );
			$date->setTimezone( new DateTimeZone( 'Europe/Helsinki' ) );
			echo _e( 'Seuraava tarkistus: ', 'kouta-datahub' ) . $date->format( 'd.m.Y H:i:s' ) . "\n";
			?>
		<?php endif; ?>
		</div>

		<div class="log" style="margin: 2em 0;">
			<p>Loki:</p>
			<?php $upload_dir = wp_upload_dir(); ?>
			<textarea name="" id="" rows="20" style="width:100%" readonly><?php echo file_get_contents( $upload_dir['basedir'] . '/datahub.log' ); ?></textarea>
		</div>

	</form>

</div>
