<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/admin
 * @author     Miika Salo <miika@koutamedia.fi>
 */
class Kouta_Datahub_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/kouta-datahub-admin.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'passed', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		) );

	}

	/**
	 * Register Kouta Databub plugins menu
	 *
	 * @since 1.0.0
	 */
	public function register_menu_page() {
		add_submenu_page(
			'options-general.php',
			'Datahub Asetukset',
			'Datahub Asetukset',
			'manage_options',
			'kouta-datahub',
			array( $this, 'load_admin_page_content' ),
		);
	}

	/**
	 * Load admin menu view
	 */
	public function load_admin_page_content() {
		require_once KOUTA_DATAHUB_PLUGIN_DIR . '/admin/partials/kouta-datahub-settings-view.php';
	}

	/**
	 * Register settings for the plugin
	 */
	public function register_settings() {
		register_setting(
			'kouta-datahub-settings',
			'kouta-datahub-access-key',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'kouta-datahub-settings',
			'kouta-datahub-api-username',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'kouta-datahub-settings',
			'kouta-datahub-api-password',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'kouta-datahub-settings',
			'kouta-datahub-api-token',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'kouta-datahub-settings',
			'kouta-datahub-product-uuids',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'kouta-datahub-settings',
			'kouta-datahub-force-update'
		);
	}

	/**
	 * Flush rewrite rules if the previously added flag exists,
	 * and then remove the flag.
	 *
	 * @since 1.0.0
	 */
	public function maybe_flush_rewrites() {
		if ( get_option( 'kouta_datahub_flush_rewrite_rules_option' ) ) {
			flush_rewrite_rules();
			delete_option( 'kouta_datahubt_flush_rewrite_rules_option' );
		}
	}

}
