<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 * @author     Miika Salo <miika@koutamedia.fi>
 */
class Kouta_Datahub {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Kouta_Datahub_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'KOUTA_DATAHUB_VERSION' ) ) {
			$this->version = KOUTA_DATAHUB_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'kouta-datahub';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		// $this->define_public_hooks();
		$this->define_integration_hooks();
		$this->define_product_hooks();
		$this->define_company_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Kouta_Datahub_Loader. Orchestrates the hooks of the plugin.
	 * - Kouta_Datahub_i18n. Defines internationalization functionality.
	 * - Kouta_Datahub_Admin. Defines all hooks for the admin area.
	 * - Kouta_Datahub_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * Plugin helper functions.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/kouta-datahub-functions.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-kouta-datahub-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-kouta-datahub-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-kouta-datahub-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-kouta-datahub-public.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-async-request.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-background-process.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-kouta-datahub-products-process.php';

		/**
		 * Include registered taxonomies.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'taxonomies/location.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'taxonomies/product_cat.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'taxonomies/product_type.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'taxonomies/season.php';

		/**
		 * The class responsible for datahub integration.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-kouta-datahub-integration.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-kouta-datahub-product.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-kouta-datahub-company.php';

		$this->loader = new Kouta_Datahub_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Kouta_Datahub_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Kouta_Datahub_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Kouta_Datahub_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_settings' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'register_menu_page' );
		$this->loader->add_action( 'init', $plugin_admin, 'maybe_flush_rewrites' );


	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Kouta_Datahub_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the Datahub integration.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_integration_hooks() {

		$plugin_integration = new Kouta_Datahub_Integration( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_ajax_get_datahub_products', $plugin_integration, 'datahub_api_call' );
		$this->loader->add_action( 'datahub_api_call_event', $plugin_integration, 'datahub_api_call' );
		$this->loader->add_action( 'datahub_api_access_token', $plugin_integration, 'save_datahub_access_token' );
		$this->loader->add_action( 'wp_ajax_get_datahub_access_token', $plugin_integration, 'get_datahub_access_token' );

	}

	/**
	 * Register all of the hooks related to the Datahub integration.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_product_hooks() {

		$plugin_product = new Kouta_Datahub_Product( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'init', $plugin_product, 'register' );
		$this->loader->add_action( 'add_meta_boxes', $plugin_product, 'register_meta_boxes' );
		$this->loader->add_filter( 'single_template', $plugin_product, 'load_template' );
		$this->loader->add_action( 'kouta_datahub_before_main_content', $plugin_product, 'kouta_datahub_output_content_wrapper', 5 );
		$this->loader->add_action( 'kouta_datahub_after_main_content', $plugin_product, 'kouta_datahub_output_content_wrapper_close', 5 );
		$this->loader->add_action( 'kouta_datahub_before_single_product_summary', $plugin_product, 'datahub_template_single_title', 5 );
		$this->loader->add_action( 'kouta_datahub_single_product_summary', $plugin_product, 'datahub_template_single_content', 5 );
		$this->loader->add_action( 'kouta_datahub_single_product_meta', $plugin_product, 'datahub_product_meta', 10 );
		$this->loader->add_action( 'kouta_datahub_after_single_product_summary', $plugin_product, 'load_product_images', 5, 1 );
		$this->loader->add_action( 'wp_trash_post', $plugin_product, 'delete_dh_product_images', 10 );

	}

	/**
	 * Register all of the hooks related to the Datahub integration.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_company_hooks() {

		$plugin_company = new Kouta_Datahub_Company( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'init', $plugin_company, 'register' );
		$this->loader->add_action( 'add_meta_boxes', $plugin_company, 'register_meta_boxes' );
		$this->loader->add_filter( 'manage_dh_company_posts_columns', $plugin_company, 'add_company_columns' );
		$this->loader->add_filter( 'manage_dh_company_posts_custom_column', $plugin_company, 'company_columns_data', 10, 2 );
		// $this->loader->add_action( 'datahub_update_company_event', $plugin_company, 'datahub_update_company' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Kouta_Datahub_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
