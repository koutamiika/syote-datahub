<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 * @author     Miika Salo <miika@koutamedia.fi>
 */
class Kouta_Datahub_Deactivator {

	/**
	 * Run on plugin deactivation.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
