<?php

function datahub_strings( $string ) {
	$strings = array(
		'monday'    => __( 'Monday', 'kouta-datahub' ),
		'tuesday'   => __( 'Tuesday', 'kouta-datahub' ),
		'wednesday' => __( 'Wednesday', 'kouta-datahub' ),
		'thursday'  => __( 'Thursday', 'kouta-datahub' ),
		'friday'    => __( 'Friday', 'kouta-datahub' ),
		'saturday'  => __( 'Saturday', 'kouta-datahub' ),
		'sunday'    => __( 'Sunday', 'kouta-datahub' ),
		'closed'    => __( 'Closed', 'kouta-datahub' ),
		'hours'     => __( 'Hours', 'kouta-datahub' ),
		'minutes'   => __( 'Minutes', 'kouta-datahub' ),
		'days'      => __( 'Days', 'kouta-datahub' ),
	);

	if ( ! empty( $strings[ $string ] ) ) {
		return esc_html( $strings[ $string ] );
	}

}

function datahub_get_languages() {
	if ( ! function_exists( 'pll_languages_list' ) ) {
		$language = get_locale();

		return (array) $language;
	}

	$languages = pll_languages_list();

	return $languages;
}

function datahub_product_hours() {

	$hours          = array_reverse( get_post_meta( get_the_ID(), 'opening_hours', true ) );
	$business_hours = get_post_meta( get_the_ID(), 'business_hours', true );
	$exception      = ! empty( $business_hours['exceptions'] ) ? $business_hours['exceptions'] : '';

	$hours_list = '';
	if ( ! empty( $business_hours['default'] ) ) {
		foreach ( $business_hours['default'] as $hour ) {

			if ( null === $hour['opens'] && null === $hour['closes'] ) {
				continue;
			}

			$hours_list .= '<li><span>' . datahub_strings( $hour['weekday'] ) . '</span><span>' . ( $hour['open'] ? substr( $hour['opens'], 0, -3 ) . '-' . substr( $hour['closes'], 0, -3 ) : datahub_strings( 'closed' ) ) . '</span></li>';
		}
	} elseif ( $hours ) {

		foreach ( $hours as $hour ) {

			if ( null === $hour['openFrom'] && null === $hour['openTo'] ) {
				continue;
			}

			$hours_list .= '<li><span>' . datahub_strings( $hour['weekday'] ) . '</span><span>' . ( $hour['open'] ? substr( $hour['openFrom'], 0, -6 ) . '-' . substr( $hour['openTo'], 0, -6 ) : datahub_strings( 'closed' ) ) . '</span></li>';
		}

	}

	return wp_kses_post( $hours_list );

}

function datahub_product_hours_exceptions() {

	$business_hours = get_post_meta( get_the_ID(), 'business_hours', true );
	$exceptions     = ! empty( $business_hours['exceptions'] ) ? $business_hours['exceptions'] : '';

	$hours_list = '';
	if ( $exceptions ) {
		foreach ( $exceptions as $exception ) {
			if ( date('U') < date( 'd.m.Y', strtotime( $exception['start'] ) ) && date('U') < date( 'd.m.Y', strtotime( $exception['end'] ) ) ) {
				continue;
			}

			echo '<p><small>' . sprintf( '%1$s %2$s-%3$s', __( 'Exceptional opening hours valid:', 'kouta-datahub' ), date( 'd.m.Y', strtotime( $exception['start'] ) ), date( 'd.m.Y', strtotime( $exception['end'] ) ) ) . '</small></p>';

			$hours_list .= '<ul>';
			foreach ( $exception['openingHours'] as $hour ) {
				if ( null === $hour['opens'] && null === $hour['closes'] ) {
					continue;
				}

				$hours_list .= '<li><span>' . datahub_strings( $hour['weekday'] ) . '</span><span>' . ( $hour['open'] ? substr( $hour['opens'], 0, -3 ) . '-' . substr( $hour['closes'], 0, -3 ) : datahub_strings( 'closed' ) ) . '</span></li>';
			}
			$hours_list .= '</ul>';
		}
	}

	return wp_kses_post( $hours_list );

}

/**
 * Write an entry to a log file in the uploads directory.
 *
 * @since x.x.x
 *
 * @param mixed $entry String or array of the information to write to the log.
 * @param string $file Optional. The file basename for the .log file.
 * @param string $mode Optional. The type of write. See 'mode' at https://www.php.net/manual/en/function.fopen.php.
 * @return boolean|int Number of bytes written to the lof file, false otherwise.
 */
if ( ! function_exists( 'datahub_log' ) ) {
	function datahub_log( $entry, $mode = 'a', $file = 'datahub' ) {
		// Get WordPress uploads directory.
		$upload_dir = wp_upload_dir();
		$upload_dir = $upload_dir['basedir'];

		// If the entry is array, json_encode.
		if ( is_array( $entry ) ) {
			$entry = wp_json_encode( $entry );
		}

		// Write the log file.
		$file  = $upload_dir . '/' . $file . '.log';
		$file  = fopen( $file, $mode );
		$bytes = fwrite( $file, current_time( 'mysql' ) . " :: " . $entry . "\n" );
		fclose( $file );

		return $bytes;
	}
}
