<?php

/**
 * The Datahub Integration functionality of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 */

/**
 * The Datahub Integration functionality of the plugin.
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 * @author     Miika Salo <miika@koutamedia.fi>
 */
class Kouta_Datahub_Integration {

	/**
	 * @var Kivi_Background_Process
	 */
	protected $process;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Datahub access token.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $access_token    Access token to use the API.
	 */
	private $access_token;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name  = $plugin_name;
		$this->version      = $version;
		$this->access_token = get_option( 'kouta-datahub-api-token' );
		$this->process      = new Kouta_Datahub_Products_Process();

		if ( ! wp_next_scheduled( 'datahub_api_call_event' ) ) {
			wp_schedule_event( time(), 'twicedaily', 'datahub_api_call_event' );
		}

		if ( ! wp_next_scheduled( 'datahub_api_access_token' ) ) {
			wp_schedule_event( time(), 'hourly', 'datahub_api_access_token' );
		}

	}

	public function save_datahub_access_token() {

		$token = $this->get_datahub_access_token(); // Get access token.

		if ( empty( $this->access_token ) || $this->access_token !== $token ) {

			// Save access token to options.
			update_option( 'kouta-datahub-api-token', $token );

			// Set token for this instance.
			$this->access_token = $token;
		}
	}

	public function get_datahub_access_token() {
		$response = wp_remote_post(
			'https://iam-datahub.visitfinland.com/auth/realms/Datahub/protocol/openid-connect/token',
			array(
				'headers' => array(
					'content-type' => 'application/x-www-form-urlencoded',
				),
				'body'    => array(
					'client_id'     => 'datahub-api',
					'client_secret' => get_option( 'kouta-datahub-access-key' ),
					'grant_type'    => 'password',
					'username'      => get_option( 'kouta-datahub-api-username' ),
					'password'      => get_option( 'kouta-datahub-api-password' ),
				),
			)
		);

		$response_body = json_decode( wp_remote_retrieve_body( $response ) );
		$response_code = wp_remote_retrieve_response_code( $response );
		if ( 200 === $response_code ) {
			$access_token = $response_body->access_token;
			return $access_token;
		}

	}

	/**
	 * Open connection to Datahub API
	 */
	public function datahub_api_call() {

		if ( empty( $this->access_token ) ) {
			return __( 'Missing or Invalid Access Token', 'kouta-datahub' );
		}

		if ( $this->process->is_process_already_running() ) {
			wp_send_json_error( array(
				'message' => 'Kohteiden tuonti on jo käynnissä.',
			) );
			wp_die();
		}

		$terms           = $this->get_locations();
		$ids             = explode( ',', get_option( 'kouta-datahub-product-uuids' ) );
		$dh              = array_map( 'trim', $ids );
		$locations       = array();
		$locations_query = '';
		$id_query        = '';

		if ( $ids ) {
			foreach ( $ids as $uuid ) {
				$id_query .= '{ id: { _eq: "' . $uuid . '" } },';
			}
		}

		foreach ( $terms as $term ) {
			$locations_query .= '{ city: { _ilike: "' . $term->name . '" } },';
			$locations[]      = $term->name;
		}

		// GraphQL query
		$query = 'query AllProductsByCities {
			product(
				where: {
                    _or: [
						' . $id_query . '
                        { postalAddresses: { _or: [ ' . $locations_query . ' ] } }
                    ]
                }
			) {
				id
				createdAt
				updatedAt
				type
				company {
					businessEntity {
						businessId
					}
				}
				duration
				durationType
				openingHours {
					open
					openFrom
					openTo
					weekday
				}
			  	businessHours {
					default {
						closes
						open
						opens
						weekday
					}
					exceptions {
						start
						end
						openingHours {
							closes
							date
							open
							opens
							weekday
						}
					}
				}
			  	postalAddresses {
					location
					postalCode
					streetName
					city
			  	}
			  	productAvailabilities {
					doorsOpenAt
					endDate
					endTime
					nroOfTickets
					startDate
					startTime
			 	}
			  	productCapacities {
					max
					min
			  	}
			  	productAvailableMonths {
					month
			  	}
			  	productInformations {
					description
					language
					name
					url
					webshopUrl
			  	}
			  	productImages {
					id
					copyright
					altText
					largeUrl
					originalUrl
					thumbnailUrl
					coverPhoto
			  	}
			 	productPricings {
					toPrice
					fromPrice
					pricingUnit
					pricingType
			  	}
			  	productTags {
					tag
			  	}
			  	productVideos {
					title
					url
			  	}
			  	productCertificates {
					certificateDetails {
						description
						name
						logoUrl
						websiteUrl
					}
				  	certificate
			  	}
			  	contactDetails {
					email
					phone
			  	}
			  	accessible
				socialMedia {
					socialMediaLinks {
						linkType
						verifiedLink {
							url
						}
					}
				}
			}
		}';

		$query = apply_filters( 'datahub_products_graphql_query', $query );

		/*  Make API call and decode the json response */
		$args = array(
			'headers' => array(
				'content-type'  => 'application/json',
				'Authorization' => 'Bearer ' . $this->access_token,
			),
			'body'    => wp_json_encode(
				array(
					'query' => $query,
				)
			),
		);

		$response         = wp_remote_post( 'https://api-datahub.visitfinland.com/graphql/v1/graphql', $args );
		$response_body    = wp_remote_retrieve_body( $response );
		$response_code    = wp_remote_retrieve_response_code( $response );
		$response_message = wp_remote_retrieve_response_message( $response );
		$data             = json_decode( $response_body, true );

		if ( $data && 200 === $response_code && empty( $data['errors'] ) ) {

			$items        = $data['data']['product'];
			$active_items = array();

			foreach ( $items as $item ) {

				$active_items[] = $item['id'];
				$this->process->push_to_queue( $item );

			}

			// Dispatch background process.
			$this->process->save()->dispatch();

			// Delete items that are not in the JSON.
    		$this->process->items_delete( $active_items );

			wp_send_json( array(
				'message' => 'Tuodaan kohteita.',
			) );

		}

		wp_die();

	}

	public static function get_locations() {

		$locations = get_terms(
			array(
				'taxonomy'   => 'location',
				'hide_empty' => false,
			)
		);

		return $locations;
	}

}
