<?php

/**
 * The Datahub Product Post Type.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 */

/**
 * The Datahub Product Post Type.
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 * @author     Miika Salo <miika@koutamedia.fi>
 */
class Kouta_Datahub_Product {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    private $type = 'dh_product';

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version     = $version;
	}

    /**
     * Register post type
     */
    public function register() {
        $labels = array(
            'name'               => __( 'Product', 'kouta-datahub' ),
            'singular_name'      => __( 'Product', 'kouta-datahub' ),
            'add_new'            => __( 'Add New', 'kouta-datahub' ),
            'add_new_item'       => __( 'Add New Product', 'kouta-datahub' ),
            'edit_item'          => __( 'Edit Product', 'kouta-datahub' ),
            'new_item'           => __( 'New Product', 'kouta-datahub' ),
            'all_items'          => __( 'All Products', 'kouta-datahub' ),
            'view_item'          => __( 'View Product', 'kouta-datahub' ),
            'search_items'       => __( 'Search Products', 'kouta-datahub' ),
            'not_found'          => __( 'No Products found', 'kouta-datahub' ),
            'not_found_in_trash' => __( 'No products found in Trash', 'kouta-datahub' ),
            'parent_item_colon'  => '',
            'menu_name'          => __( 'Products', 'kouta-datahub' ),
        );

        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => __( 'products', 'kouta-datahub' ) ),
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => true,
            'menu_position'      => 8,
            'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
        );

        register_post_type( $this->type, $args );
    }

    /**
	 * Register meta box(es).
	 */
	public function register_meta_boxes() {
		add_meta_box( 'product-meta', __( 'Product information', 'kouta-datahub' ), array( $this, 'meta_box_cb' ), 'dh_product' );
	}

	/**
	 * Meta box display callback.
	 *
	 * @param WP_Post $post Current post object.
	 */
	public function meta_box_cb( $post ) {

		$metas = get_post_custom( $post->ID );

		ob_start();

		include_once KOUTA_DATAHUB_PLUGIN_DIR . '/admin/partials/datahub-product-meta-fields.php';

		$body = ob_get_clean();

		echo $body;

	}

	/**
	 * Load single template
	 */
	public function load_template( $template ) {
		global $post;

		if ( 'dh_product' === $post->post_type && locate_template( 'single-dh_product.php' ) !== $template ) {
			return KOUTA_DATAHUB_PLUGIN_DIR . '/templates/single-dh_product.php';
		}

		return $template;
	}

	public function datahub_template_single_title() {
		?>
		<h1 class="datahub-title entry-title"><?php the_title(); ?></h1>
		<?php
	}

	public function datahub_product_meta() {
		?>
		<ul class="product-metas">
			<li><?php echo get_post_meta( get_the_ID(), 'webshop_url', true ); ?>
			<li><?php echo get_post_meta( get_the_ID(), 'website_url', true ); ?>
			<li><?php echo get_post_meta( get_the_ID(), 'address', true ); ?>
			<li><?php echo get_post_meta( get_the_ID(), 'zip', true ); ?>
		</ul>

		<?php
	}

	public function datahub_template_single_content() {
		the_content();
	}

	public function load_product_images( $post_id ) {
		$images = get_attached_media( 'image', $post_id );
		echo '<section class="images">';
		if ( $images ) :
			foreach ( $images as $image ) :
				echo '<div class="gallery-item"><a href="' . wp_get_attachment_image_url( $image->ID, 'full' ) . '">' . wp_get_attachment_image( $image->ID, 'thumbnail' ) . '</a></div>';
			endforeach;
		endif;
		echo '</section>';
	}

	/**
	 * Delete product
	 *
	 * @param int $post_id ID of the post to delete.
	 */
	public function delete_dh_product_images( $post_id ) {

		if ( ! $post_id ) {
			return;
		}

		if ( get_post_type( $post_id ) !== 'dh_product' ) {
			return;
		}

		$images = get_post_meta( $post_id, '_datahub_product_image' );
		foreach ( $images as $attach_id ) {
            wp_delete_attachment( $attach_id, true );
        }

		// wp_delete_post( $post_id, true );

	}

	public function kouta_datahub_output_content_wrapper() {
		echo '<div id="inner-content">';
	}

	public function kouta_datahub_output_content_wrapper_close() {
		echo '</div">';
	}

}
