<?php

/**
 * The Datahub Integration functionality of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 */

/**
 * The Datahub Integration functionality of the plugin.
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 * @author     Miika Salo <miika@koutamedia.fi>
 */
class Kouta_Datahub_Products_Process extends WP_Background_Process {

	/**
     * Action hook to start the background process
     */
    protected $action = 'datahub_sync_products';

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Datahub access token.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $access_token    Access token to use the API.
	 */
	private $access_token;

	/**
     * Called on every item on task queue.
     */
    protected function task( $item ) {
        /* Add or update item */
        $this->handle_parsed_item( $item );

        return false;
    }

    /**
     * Stop the background process.
     */
    public function stop() {
        // Delete queue
        $this->delete( $this->get_batch()->key );
    }

	/**
     * Stops the background process and deletes all imported Datahub data.
     */
    public function reset() {

        // Delete queue
        $this->delete( $this->get_batch()->key );

        // Delete items
        $this->items_delete();

    }

	/**
     * Called once after background process is done and task queue is empty
     */
    protected function complete() {
        parent::complete();

		update_option( 'task_completed', 'completed' );
    }

	/**
     * Stop multiple processes from being dispatched.
     */
    public function is_process_already_running() {
        return $this->is_process_running();
    }

	public function handle_parsed_item( &$item ) {

		$item = $this->prepare_item_for_database( $item );

		$translations = array();

		foreach ( $item as $language => $content ) {
			if ( $this->item_exists( $content['datahub_id'], $language ) ) {
				$this->item_update( $content );
				$this->company_update( $content['business_id'], $language );
			} else {
				$id = $this->item_add( $content );

				$translations[ $language ] = $id;
			}
		}

		if ( function_exists( 'pll_save_post_translations' ) ) {
			pll_save_post_translations( $translations );
		}

    }

	/**
	 * Create new post
	 */
	public function item_add( $content ) {

		if ( empty( $content ) ) {
			return;
		}

		datahub_log( $content );

		$args = array(
			'post_content'  => wpautop( $content['description'] ),
			'post_status'   => 'publish',
			'post_author'   => 1,
			'post_title'    => $content['name'],
			'post_type'     => 'dh_product',
			'post_date'     => $content['createdAt'],
			'post_modified' => $content['updatedAt'],
			'lang'          => $content['language'],
		);
		$args = apply_filters( 'datahub_insert_item_args', $args, $content );

		$post_id = wp_insert_post( $args );

		if ( ! is_wp_error( $post_id ) ) {

			if ( function_exists( 'pll_set_post_language' ) ) {
				pll_set_post_language( $post_id, $content['language'] );
			}

			$this->update_item_meta( $post_id, $content );
			$this->set_taxonomies( $post_id, $content, $content['language'] );

			if ( ! $this->company_exists( $content['business_id'], $content['language'] ) ) {
				$this->get_datahub_company( $content['business_id'], $content['language'] );
			}

			datahub_log( '(' . $post_id . ') ' . get_the_title( $post_id ) . ' created.' );
		}

		return $post_id;

	}

	/**
	 * Get post's all Polylang translations.
	 */
	public function get_translations( $post_id ) {

		if ( ! $post_id ) {
			return;
		}

		if ( ! function_exists( 'pll_get_post_translations' ) ) {
			return;
		}

		$translations = pll_get_post_translations( $post_id );

		return $translations;

	}

	public function set_taxonomies( $post_id, $content, $language = 'en' ) {

		if ( ! $post_id ) {
			return;
		}

		if ( ! $content ) {
			return;
		}

		$type = $content['type'];

		if ( $type ) {
			wp_set_object_terms( $post_id, ucfirst( str_replace( '_', ' ', $type ) ), 'product_type' );
		}

		$location = $content['city'];

		if ( $location ) {
			if ( 'Syöte' === $location || 'Pudasjärvi' === $location ) {
				wp_set_object_terms( $post_id, $location, 'location' );
			}
		}

		$seasons = wp_list_pluck( $content['seasons'], 'month' );

		if ( $seasons ) {
			wp_set_object_terms( $post_id, $seasons, 'season' );
		}

		$tags = wp_list_pluck( $content['tags'], 'tag' );

		/**
		 * Array of translated category tags.
		 */
		$cats = array();

		if ( $tags ) {
			foreach ( $tags as $tag ) {
				$translated_tag = datahub_tag( $tag, $language );
				$cats[]         = $translated_tag;
			}

			/**
			 * Set category terms for post. Will delete difference of terms.
			 */
			$cat = wp_set_object_terms( $post_id, $cats, 'product_cat' );

			foreach ( $cat as $c ) {
				if ( ! term_exists( $c, 'product_cat' ) ) {
					if ( function_exists( 'pll_set_term_language' ) ) {
						pll_set_term_language( $c, $language );
					}
				}
			}
		}

	}

	public function update_item_meta( $post_id, $content ) {

		$args = array(
			'ID'            => $post_id,
			'post_title'    => $content['name'],
			'post_content'  => wpautop( $content['description'] ),
			'post_modified' => $content['updatedAt'],
		);

		$updated_post = wp_update_post( $args );

		$metas = array(
			'datahub_id'     => $content['datahub_id'],
			'business_id'    => $content['business_id'],
			'updatedAt'      => $content['updatedAt'],
			'webshop_url'    => $content['webshopUrl'],
			'website_url'    => $content['url'],
			'duration'       => $content['duration'],
			'duration_type'  => $content['duration_type'],
			// 'opening_hours'  => $content['opening_hours'], DEPRACATED
			'business_hours' => $content['business_hours'],
			'coordinates'    => $content['coordinates'],
			'address'        => $content['address'],
			'zip'            => $content['zip'],
			'pricing'        => $content['pricing'],
			'accessible'     => $content['accessible'],
			'videos'         => $content['videos'],
			'phone'          => $content['phone'],
			'email'          => $content['email'],
			'certificates'   => $content['certificates'],
			'capacities'     => $content['capacities'],
			'social_links'   => $content['social_links'],
		);

		$images = $content['images'];

		$metas = apply_filters( 'datahub_metas', $metas );

		foreach ( $metas as $key => $value ) {
			update_post_meta( $post_id, $key, $value );
		}

		$additional_images = array();

		if ( $images ) {
			foreach ( $images as $key => $value ) {
				if ( ! $this->image_exists( $value['id'], $content['language'] ) ) {
					if ( true === $value['coverPhoto'] ) {
						$url = str_replace( '?s=1920', '', $value['largeUrl'] );
						$this->save_image( $url, 'MAIN', $post_id, $value['altText'], $value['id'] );
					} else {
						$additional_images[] = array(
							'id'        => $value['id'],
							'thumbnail' => $value['thumbnailUrl'],
							'url'       => str_replace( '?s=1920', '', $value['largeUrl'] ),
							'alt'       => $value['altText'],
						);
					}
				}
			}
		}

		if ( ! empty( $additional_images ) ) {
			update_post_meta( $post_id, 'additional_images', $additional_images );
		}

	}

	/**
	 * Check if the item already exists in wp database
	 */
	public function item_exists( $id, $language ) {
		$args = array(
			'meta_key'    => 'datahub_id',
			'meta_value'  => $id,
			'post_type'   => 'dh_product',
			'post_status' => 'any',
			'lang'        => $language,
		);

		$count = count( get_posts( $args ) );

		return $count > 0;
	}

	/**
	 * Figure out if item needs to be modified. That is if the updatedate in the
	 * post metadata is different from the one in the incoming JSON.
	 */
	public function company_update( $business_id, $language ) {

		$args = array(
			'meta_key'   => 'datahub_id',
			'meta_value' => $business_id,
			'post_type'  => 'dh_company',
			'lang'       => $language,
		);

		$posts = get_posts( $args );

		if ( ! isset( $posts[0] ) ) {
			return;
		}

		$post = $posts[0];

		$updated = get_post_meta( $post->ID, 'updated', true );

		if ( $content['updatedAt'] !== $updated ) {
			$this->get_datahub_company( $business_id, $language, $post->ID );
		}

	}

	/**
	 * Figure out if item needs to be modified. That is if the updatedate in the
	 * post metadata is different from the one in the incoming JSON.
	 */
	public function item_update( $content ) {

		$args = array(
			'meta_key'   => 'datahub_id',
			'meta_value' => $content['datahub_id'],
			'post_type'  => 'dh_product',
			'lang'       => $content['language'],
		);

		$posts = get_posts( $args );

		if ( ! isset( $posts[0] ) ) {
			return;
		}

		$post = $posts[0];

		$force = get_option( 'kouta-datahub-force-update' );

		$updated = '';

		if ( empty( $force ) ) {
			$updated = get_post_meta( $post->ID, 'updatedAt', true );
		}

		if ( $content['updatedAt'] !== $updated ) {
			$this->update_item_meta( $post->ID, $content );
			$this->set_taxonomies( $post->ID, $content, $content['language'], false );
			datahub_log( '(' . $post->ID . ') ' . get_the_title( $post->ID ) . ' updated.' );
		}

	}

	/**
	 * Delete item
	 *
	 * @param int $post_id ID of the post to delete.
	 */
	public function item_delete( $post_id ) {

		if ( ! $post_id ) {
			return;
		}

		$args = array(
			'meta_key'   => 'business_id',
			'meta_value' => get_post_meta( $post_id, 'business_id', true ),
			'post_type'  => 'dh_company',
			'lang'       => $lang,
		);

		$companies = get_posts( $args );

		if ( $companies ) {
			foreach ( $companies as $company ) {
				wp_delete_post( $company->ID, true );
				datahub_log( 'Company ' . $company->ID . ' was deleted.' );
			}
		}

		$images = get_post_meta( $post_id, '_datahub_product_image' );

		foreach ( $images as $attach_id ) {
            wp_delete_attachment( $attach_id, true );
        }

		wp_delete_post( $post_id, true );

		datahub_log( 'Item ' . $post_id . ' was deleted.' );

	}

	public function items_delete( &$items = array() ) {

		$args = array(
			'meta_key'    => 'datahub_id',
			'post_type'   => 'dh_product',
			'numberposts' => -1,
		);

		$posts = get_posts( $args );

		foreach ( $posts as $post ) {

			$datahub_id = get_post_meta( $post->ID, 'datahub_id', true );

			if ( ! in_array( $datahub_id, $items, true ) ) {

				$this->item_delete( $post->ID );

			}
		}

	}

	/**
	 * Check if product image exists.
	 */
	public function image_exists( $uuid, $lang ) {
		$args = array(
			'meta_key'   => 'uuid',
			'meta_value' => $uuid,
			'post_type'  => 'attachment',
			'lang'       => $lang,
		);

		$count = count( get_posts( $args ) );

		return $count > 0;
	}

	/*
	* Save image into media library as an attachment to the according Product
	*/
	public function save_image( $url, $image_type, $post_id = 0, $alt = '', $uuid ) {

		$filename       = basename( $url );
		$upload_dir     = wp_upload_dir();
		$upload_basedir = $upload_dir['basedir'];
		$filepath       = $upload_basedir . '/' . $filename;

		if ( file_exists( $filepath ) ) {
			datahub_log( 'Image ' . $filepath . ' exists. Skipping image.' );
			return;
			// $filename = $filename . '-' . sha1( $filename );
		}

		$upload_file = wp_upload_bits( $filename, null, wp_remote_retrieve_body( wp_remote_get( $url ) ) );

		if ( ! $upload_file['error'] ) {

			$wp_filetype = wp_check_filetype( $filename, null );
			$attachment  = array(
				'post_mime_type' => $wp_filetype['type'],
				'post_parent'    => $post_id,
				'post_title'     => preg_replace( '/\.[^.]+$/', '', $filename ),
				'post_content'   => '',
				'post_status'    => 'inherit',
			);

			$attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $post_id );
			if ( $attachment_id ) {

				// Set featured image if not yet set.
				if ( 'MAIN' === $image_type ) {
					set_post_thumbnail( $post_id, $attachment_id );
				}

				require_once ABSPATH . 'wp-admin/includes/image.php';
				$attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
				wp_update_attachment_metadata( $attachment_id,  $attachment_data );

				update_post_meta( $attachment_id, '_wp_attachment_image_alt', $alt );
				add_post_meta( $post_id, '_datahub_product_image', $attachment_id );
				update_post_meta( $attachment_id, 'uuid', $uuid );
				add_post_meta( $attachment_id, 'original_image_url', $url );
				add_post_meta( $attachment_id, 'image_type', $image_type );

				wp_set_object_terms( $attachment_id, 'tuotekuvat', 'media_category' );

				return wp_get_attachment_url( $attachment_id );
			}
		}
	}

	/**
	 * GraphQL query to fetch company information from datahub.
	 */
	public function get_datahub_company( $business_id, $lang = 'en', $id = null ) {

		// GraphQL query
		$query = 'query GetCompanies {
			company (where: {businessEntity: {businessId: {_eq: "' . $business_id . '"}}}) {
				updatedAt
				id
				logoThumbnailUrl
				logoUrl
				businessName
				officialName
				webshopUrl
				websiteUrl
				postalAddresses {
					city
					location
					postalCode
					streetName
				}
				socialMediaLinks {
					linkType
					url
				}
				contactDetails {
					email
					phone
				}
				description
				businessEntity {
					businessId
				}
			}
		}';

		$query = apply_filters( 'datahub_companies_graphql_query', $query );

		$access_token = get_option( 'kouta-datahub-api-token' );

		/*  Make API call and decode the json response */
		$args = array(
			'headers' => array(
				'content-type'  => 'application/json',
				'Authorization' => 'Bearer ' . $access_token,
			),
			'body'    => wp_json_encode(
				array(
					'query' => $query,
				)
			),
		);

		$response         = wp_remote_post( 'https://api-datahub.visitfinland.com/graphql/v1/graphql', $args );
		$response_body    = wp_remote_retrieve_body( $response );
		$response_code    = wp_remote_retrieve_response_code( $response );
		$response_message = wp_remote_retrieve_response_message( $response );
		$data             = json_decode( $response_body, true );

		if ( $data && 200 === $response_code && empty( $data['errors'] ) ) {
			// Create an array with groupName as key and an array of tags as value.
			$companies = $data['data']['company'];

			// Loop through category groups and insert them as terms.
			foreach ( $companies as $company ) {

				$args    = array(
					'post_content' => $company['description'],
					'post_status'  => 'publish',
					'post_title'   => $company['businessName'],
					'post_date'    => $company['updatedAt'],
					'post_type'    => 'dh_company',
				);

				if ( null !== $id ) {
					$args['ID'] = (int) $id;
				}

				$post_id = wp_insert_post( $args );

				$metas = array(
					'id'            => $company['id'],
					'updated'       => $company['updatedAt'],
					'business_id'   => $company['businessEntity']['businessId'],
					'business_name' => $company['businessName'],
					'webshop_url'   => $company['webshopUrl'],
					'website_url'   => $company['websiteUrl'],
					'street_name'   => $company['postalAddresses'][0]['streetName'],
					'location'      => $company['postalAddresses'][0]['location'],
					'zip_address'   => $company['postalAddresses'][0]['postalCode'] . ', ' . $company['postalAddresses'][0]['city'],
					'social_links'  => $company['socialMediaLinks'],
					'company_email' => $company['contactDetails'][0]['email'],
					'company_phone' => $company['contactDetails'][0]['phone'],
				);

				if ( ! is_wp_error( $post_id ) ) {

					if ( function_exists( 'pll_set_post_language' ) ) {
						pll_set_post_language( $post_id, $lang );
					}

					foreach ( $metas as $key => $value ) {
						update_post_meta( $post_id, $key, $value );
					}

					if ( null === $id ) {
						datahub_log( 'Company ' . $post_id. ' created.' );
					} else {
						datahub_log( 'Company ' . $post_id. ' updated.' );
					}

				}
			}
		}

	}

	/**
	 * Check if company exists in the database based on their business id (y-tunnus).
	 */
	public function company_exists( $business_id, $lang ) {
		$args = array(
			'meta_key'   => 'business_id',
			'meta_value' => $business_id,
			'post_type'  => 'dh_company',
			'lang'       => $lang,
		);

		$posts = get_posts( $args );

		return $posts[0]->ID;
	}

	/**
	 * Map data to new associative array for easier handling. Language slug as key.
	 */
	public function prepare_item_for_database( $item ) {

		if ( empty( $item ) ) {
			return;
		}

		$product      = array();
		$descriptions = $item['productInformations'];

		if ( $descriptions ) {
			foreach ( $descriptions as $description ) {

				$site_langs = datahub_get_languages();

				if ( in_array( $description['language'], $site_langs, true ) ) {
					$product[ $description['language'] ] = array(
						'description'    => $description['description'],
						'name'           => $description['name'],
						'url'            => $description['url'],
						'webshopUrl'     => $description['webshopUrl'],
						'language'       => $description['language'],
						'datahub_id'     => $item['id'],
						'business_id'    => $item['company']['businessEntity']['businessId'],
						'createdAt'      => $item['createdAt'],
						'updatedAt'      => $item['updatedAt'],
						'duration'       => $item['duration'],
						'duration_type'  => $item['durationType'],
						'opening_hours'  => $item['openingHours'],
						'business_hours' => $item['businessHours'],
						'coordinates'    => substr( $item['postalAddresses'][0]['location'], 1, -1 ),
						'address'        => $item['postalAddresses'][0]['streetName'],
						'zip'            => $item['postalAddresses'][0]['postalCode'] . ', ' . $item['postalAddresses'][0]['city'],
						'city'           => $item['postalAddresses'][0]['city'],
						'pricing'        => $item['productPricings'],
						'accessible'     => $item['accessible'],
						'videos'         => $item['productVideos'],
						'phone'          => $item['contactDetails'][0]['phone'],
						'email'          => $item['contactDetails'][0]['email'],
						'certificates'   => $item['productCertificates'],
						'images'         => $item['productImages'],
						'type'           => $item['type'],
						'seasons'        => $item['productAvailableMonths'],
						'availabilities' => $item['productAvailabilities'],
						'tags'           => $item['productTags'],
						'capacities'     => $item['productCapacities'],
						'social_links'   => $item['socialMedia'],
					);
				}
			}
		}

		return $product;

	}

}
