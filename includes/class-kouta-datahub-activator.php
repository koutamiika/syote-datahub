<?php

/**
 * Fired during plugin activation
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 * @author     Miika Salo <miika@koutamedia.fi>
 */
class Kouta_Datahub_Activator {

	/**
	 * Run on plugin activation.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		if ( ! get_option( 'kouta_datahub_flush_rewrite_rules_option' ) ) {
			add_option( 'kouta_datahub_flush_rewrite_rules_option', true );
		}
	}

}
