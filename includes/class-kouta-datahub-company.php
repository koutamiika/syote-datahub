<?php

/**
 * The Datahub Company Post Type.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 */

/**
 * The Datahub Company Post Type.
 *
 * @package    Kouta_Datahub
 * @subpackage Kouta_Datahub/includes
 * @author     Miika Salo <miika@koutamedia.fi>
 */
class Kouta_Datahub_Company {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

		// if ( ! wp_next_scheduled( 'datahub_update_company_event' ) ) {
		// 	wp_schedule_event( time(), 'twicedaily', 'datahub_update_company_event' );
		// }

	}

    /**
     * Register post type
     */
    public function register() {
        $labels = array(
            'name'               => __( 'Company', 'kouta-datahub' ),
            'singular_name'      => __( 'Company', 'kouta-datahub' ),
            'add_new'            => __( 'Add New', 'kouta-datahub' ),
            'add_new_item'       => __( 'Add New Company', 'kouta-datahub' ),
            'edit_item'          => __( 'Edit Company', 'kouta-datahub' ),
            'new_item'           => __( 'New Company', 'kouta-datahub' ),
            'all_items'          => __( 'All Companies', 'kouta-datahub' ),
            'view_item'          => __( 'View Company', 'kouta-datahub' ),
            'search_items'       => __( 'Search Companies', 'kouta-datahub' ),
            'not_found'          => __( 'No Companies found', 'kouta-datahub' ),
            'not_found_in_trash' => __( 'No Companies found in Trash', 'kouta-datahub' ),
            'parent_item_colon'  => '',
            'menu_name'          => __( 'Companies', 'kouta-datahub' ),
        );

        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => __( 'company', 'kouta-datahub' ) ),
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => true,
            'menu_position'      => 8,
            'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail', 'author' ),
        );

        register_post_type( 'dh_company', $args );
    }

    /**
	 * Register meta box(es).
	 */
	public function register_meta_boxes() {
		add_meta_box( 'company-meta', __( 'Meta', 'kouta-datahub' ), array( $this, 'meta_box_cb' ), 'dh_company' );
	}

	/**
	 * Meta box display callback.
	 *
	 * @param WP_Post $post Current post object.
	 */
	public function meta_box_cb( $post ) {

		$metas = get_post_custom( $post->ID );

		ob_start();

		include_once KOUTA_DATAHUB_PLUGIN_DIR . '/admin/partials/datahub-company-meta-fields.php';

		$body = ob_get_clean();

		echo $body;

	}

	public static function company_exists( $business_id ) {

		if ( ! $business_id ) {
			return;
		}

		$args = array(
			'meta_key'   => 'business_id',
			'meta_value' => $business_id,
			'post_type'  => 'dh_company',
		);

		$count = count( get_posts( $args ) );

		return $count > 0;
	}

	public function add_company_columns( $columns ) {

		unset( $columns['title'] );
		unset( $columns['date'] );

		$columns['datahub'] = __( 'Datahub', 'kouta-datahub' );
		$columns['title']   = __( 'Title', 'kouta-datahub' );
		$columns['address'] = __( 'Address', 'kouta-datahub' );
		$columns['zip']     = __( 'Zip', 'kouta-datahub' );
		$columns['email']   = __( 'Email', 'kouta-datahub' );
		$columns['phone']   = __( 'Phone', 'kouta-datahub' );
		$columns['date']    = __( 'Date', 'kouta-datahub' );

		return $columns;
	}

	public function company_columns_data( $column_name, $post_id ) {
		switch ( $column_name ) {
			case 'datahub':
				echo ( get_post_meta( $post_id, 'id', true ) ? '<span style="background:limegreen; padding:5px; border-radius:3px;color:white;">' . esc_html__( 'Kyllä', 'kouta-datahub' ) . '</span>' : '<span style="background:deepskyblue;color: white;padding:5px;border-radius:3px;">' . esc_html__( 'Manuaalinen', 'kouta-datahub' ) . '</span>');
				break;
			case 'address':
				echo get_post_meta( $post_id, 'street_name', true ) ;
				break;
			case 'zip':
				echo get_post_meta( $post_id, 'zip_address', true );
				break;
			case 'email':
				echo get_post_meta( $post_id, 'company_email', true );
				break;
			case 'phone':
				echo get_post_meta( $post_id, 'company_phone', true );
				break;
			default:
				break;
		}
	}

	// public function datahub_update_company() {

	// }
}
